Soundroid

#Détails concernant le manifest modifié 
J'ai ajouté les paramètres suivant dans la balise "<activity..." afin d'avoir une app en plein écran et bloquée en mode paysage.
android:screenOrientation="landscape"
android:theme="@android:style/Theme.NoTitleBar.Fullscreen"

============================================================================================================

#Architecture de base

Une activité contient les éléments suivant

- Une classe héritant de Activity (exemple : MainActivity), c'est la classe de base de l'activité, on y créé le controlleur principal de l'activité.
- Une classe implémentant l'interface Controller (exemple : MainController), c'est le controlleur de l'activité, on y créé le principal modèle (ex: GridModel), la vue principale (ex: MainView) ainsi qu'un gestionnaire d'évènement.
  C'est aussi dans cette classe qu'on connecte chaque élément à un évènement (ici, chaque bouton à la méthode à appeler en cas de clique, (ex : à l'aide de la classe MainEventListener)
- Un ou plusieurs modèles, exemple : La classe GridModel gère la grille, créé tous les boutons...
- Une classe pour gérer la vue, exemple : MainView dessine les boutons dans le layout principal.
- Une classe pour gérer les évènements, exemple : MainEventListener, elle utilise une classe de base appelée MyOnTouchListener (décrite plus bas) qui nous permet d'avoir accès au controlleur passé en param.

Element en commun à chaque activité

- Interface Controller : chaque controlleur implémente cette interface, ce qui permet de faire passer n'importe quel controlleur dans la gestion des évènements.
- MyOnTouchListener : gestion personnalisée des évènements 'OnTouch', se basant sur ceux d'android mais permettant d'utiliser le controlleur passé en param :P.

============================================================================================================

#Détails concernant les activités
Pour afficher une nouvelle vue, on peut créer une nouvelle activitée.

Notes

- Quand on passe de l'activité 1 à l'activité 2, l'état de l'activité 1 n'est pas sauvegardé.
- On peut faire passer des données d'une activité à l'autre.

Tuto pour créer une activité : http://www.tutos-android.com/changement-vues-passage-donnees-android

============================================================================================================

#Tester le premier pack de Son 
[Old - Ne fonctionne plus - En attente de Partition]

1. Pour tester le premier pack de son, récuperer le contenu de la branch Son qui possède les dernières modifications
2. Une fois l'application mise à jour, télécharger le pack juste ici: [https://drive.google.com/file/d/0B79r2m67OFwqTEIwWnJZWXg0T2c/view?usp=sharing](https://drive.google.com/file/d/0B79r2m67OFwqTEIwWnJZWXg0T2c/view?usp=sharing)
3. Décompressez l'archive à la ***racine*** de votre tablette
4. Lancez l'application, cliquez sur le bouton Load, lancer l'appli de selection de fichier et choississez le fichier map.xml présent à la racine de votre tablette
5. Jouez :D