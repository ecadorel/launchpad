package univ.etu.soundroid.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.Note;
import univ.etu.soundroid.Models.Vector2;

/**
 * Created by emile on 30/11/2015.
 */
public class NoteController extends Controller {

    private static volatile NoteController instance = null;

    public static NoteController getInstance() {
        if(instance == null) {
            instance = new NoteController();
        }
        return instance;
    }

    private class NotePlayingInfo {
        Note note = null;
        boolean hasBegin = false;
        double beginTime = -1.0;
        boolean mustStop = false;
        int sound_instance = -1;
        boolean isInPartition = false;
        double length = -1;
        static final float SCALE = 10.0f;
    }

    private Map<Integer, NotePlayingInfo> infos = new HashMap<>();

    private NoteController() {
    }

    public void playNote(int id, double time) {
        Note n = GlobalData.getInstance().getGrid().getNote(id);
        if(n != null) {
            GridPlayController.getInstance().setButtonPressed(true, n.getData().getColor(), n.getId());
        }

        NotePlayingInfo info = infos.get(n.getId());

        if(info == null) {
            info = new NotePlayingInfo();
            info.note = n;
            infos.put(n.getId(), info);
        }

        if(!n.getData().isPartition()) {
            playSound(info, time);
        } else {
            playPartition(info, time);
        }
    }

    public void playFor(NotePlayingInfo npi, double time, double length) {
        if(npi != null) {
            GridPlayController.getInstance().setButtonPressed(true, npi.note.getData().getColor(), npi.note.getId());
        }

        npi.beginTime = time;
        npi.length = length;
        npi.isInPartition = true;
        GridPlayController.getInstance().getConductor().addSoundToPlayInThread(npi.note.getId());

    }

    public void playSound(NotePlayingInfo npi, double time) {

        if(!npi.isInPartition || time - npi.beginTime < npi.length) {
            if (!npi.hasBegin) {
                npi.sound_instance = SoundController.getInstance().play(npi.note.getData().getSoundId());
                npi.hasBegin = true;
            }
        }
    }

    public void stop(int id) {
        NotePlayingInfo npi = infos.get(id);
        if(npi != null) {
            if (npi.hasBegin && !npi.note.getData().isPartition()) {
                SoundController.getInstance().stop(npi.sound_instance);
            }

            GridPlayController.getInstance().setButtonPressed(false, npi.note.getData().getColor(), npi.note.getId());
            infos.remove(npi.note.getId());
        }
    }


    public void playPartition(NotePlayingInfo npi, double time) {
        if(!npi.hasBegin) {
            npi.hasBegin = true;
            npi.beginTime = time;

        }
        double localTime = (time - npi.beginTime) / npi.SCALE;

        System.out.println(localTime);
        //On part du principe qu'il n'y a plus rien a jouer, on verifie en jouant justement !!
        npi.mustStop = true;

        for(Map.Entry<Integer, List<Vector2>> data : npi.note.getData().getPartition().getPartitionData().entrySet()) {
            for(Vector2 pos : data.getValue()) {
                if(localTime >= pos.getX() && localTime <= (pos.getY() + pos.getX())) {
                    //On joue la note qui est dans l'intervalle
                    npi.mustStop = false;
                    NotePlayingInfo inInfo = infos.get(data.getKey());
                    if(inInfo == null) {
                        inInfo = new NotePlayingInfo();
                        //Cette methode est thread Safe
                        Note n = GridPlayController.getInstance().getConductor().getNoteById(data.getKey());
                        inInfo.note = n;
                        infos.put(n.getId(), inInfo);
                        //On verifie qu'on a pas deja lancer la note, sinon on va la lancer pos.second fois ^^
                        playFor(inInfo, time, pos.getY() * npi.SCALE);
                    }
                }
                //On est pas arriver en fin de partition, il reste des trucs a jouer
                if(localTime < pos.getX()) npi.mustStop = false;
            }
        }
    }

    public boolean isEnd(int id, double time) {
        NotePlayingInfo npi = infos.get(id);
        if(npi != null) {
            if (npi.isInPartition) {
                if (time - npi.beginTime > npi.length && npi.beginTime != -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return npi.mustStop;
            }
        }
        return true;
    }

    public boolean isOnlySound(int id) {
        NotePlayingInfo info = infos.get(id);
        if(info != null) {
            return !info.note.getData().isPartition();
        }
        return true;
    }

}
