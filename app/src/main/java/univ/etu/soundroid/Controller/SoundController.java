package univ.etu.soundroid.Controller;

import android.app.Activity;
import android.media.MediaPlayer;

import univ.etu.soundroid.Models.Sound;
import univ.etu.soundroid.Models.SoundManager;

/**
 * Created by emile on 29/11/2015.
 */
public class SoundController extends Controller {

    private static volatile SoundController instance = null;

     public static SoundController getInstance() {
        if(instance == null) {
            instance = new SoundController();
        }
        return instance;
    }


    public int play(int idSound) {
       try {
            Sound s = SoundManager.getInstance().getSound(idSound);
            if (s!=null && (s.getIdResource()!=-1 || s.getPath()!=null)) {
                try {
                    MediaPlayer mp;
                    if(s.isOnSdcard()){
                        mp = new MediaPlayer();
                        mp.setDataSource(s.getPath());
                        mp.prepare();
                    }else{
                        mp = MediaPlayer.create(getActivity(),s.getIdResource());
                    }
                    mp.setLooping(true);

                    SoundManager.getInstance().addInstance(mp);
                    mp.start();
                    return SoundManager.getInstance().getIdCount();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch(IndexOutOfBoundsException e){}
        return -1;
    }

    public void stop(int idInstance) {
        SoundManager.getInstance().stop(idInstance);
    }

    public void set(Activity activity) {
        super.setActivity(activity);
    }


}
