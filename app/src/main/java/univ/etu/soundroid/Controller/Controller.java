package univ.etu.soundroid.Controller;

import android.app.Activity;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import univ.etu.soundroid.Models.FileTool;
import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.IO.DocumentMalformedException;
import univ.etu.soundroid.Models.IO.XMLStream;
import univ.etu.soundroid.Models.SoundroidAppData;

/**
 * Created by emile on 28/11/2015.
 */
public class Controller {

    private Activity currentActivity = null;

    protected int      screenWidth  = 0;
    protected int      screenHeight = 0;

    public void setActivity(Activity act) {
        currentActivity = act;
    }

    public void setSize(int w, int h) {
        screenWidth = w;
        screenHeight = h;
    }

    public Activity getActivity() {
        return currentActivity;
    }


    /**
     * Sauvegarde les informations du projet dans un fichier
     * */
    public void save() throws ParserConfigurationException {
        try {
            File file = GlobalData.getInstance().getProjectFile().getFile();
            XMLStream stream = new XMLStream();
            stream.createStream(file, GlobalData.getInstance());
            FileTool.registerFile(currentActivity ,file.getPath());
        }catch (TransformerException e) {
            e.printStackTrace();
        }

        SoundroidAppData.addRecentProject(GlobalData.getInstance().getProjectFile());
    }


    public void open(String path) throws ParserConfigurationException, DocumentMalformedException, SAXException, IOException {
        File file = new File(path);
        GlobalData.getInstance().setProjectFile(file);
        XMLStream stream = new XMLStream();
        stream.openStream(file);
        stream.deserialize(GlobalData.getInstance());
    }


}
