package univ.etu.soundroid.Controller;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Models.Note;
import univ.etu.soundroid.Models.Vector2;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.view.NoteResizer;
import univ.etu.soundroid.View.Partition.view.NoteView;
import univ.etu.soundroid.View.Partition.view.PartitionBin;
import univ.etu.soundroid.View.Partition.activity.NoteEditActivity;
import univ.etu.soundroid.View.Partition.Event.PartitionPageEventListener;
import univ.etu.soundroid.View.Partition.view.PartitionBottomScroll;
import univ.etu.soundroid.View.Partition.view.PartitionLineView;

/**
 * Created by emile on 28/11/2015.
 */
public class PartitionPageController extends Controller {

    private static volatile PartitionPageController instance = null;
    private PartitionPageEventListener eventListener = null;
    private Note data = null;
    private Button ajouterLigne = null;
    private ScrollView scrollView = null;
    private TableLayout tableLigne = null;
    private PartitionBin partitionBin = null;
    private PartitionBottomScroll horizontalScroll = null;
    private NoteView selection = null;
    private EditText longueurView = null;
    private EditText positionView = null;
    private EditText totalView = null;
    private PartitionLineView lineSelected = null;


    public static PartitionPageController getInstance() {
        if(instance == null) {
            instance = new PartitionPageController();
        }
        return instance;
    }

    public void set(NoteEditActivity activity) {
        super.setActivity(activity);
        eventListener = PartitionPageEventListener.getInstance();
        partitionBin.setOnDragListener(eventListener.getOnDragListener());


    }

    public PartitionPageEventListener getEventListener() {
        return eventListener;
    }

    public void setData(Note data) {
        this.data = data;
        ajouterLigne = (Button)getActivity().findViewById(R.id.AjouterLigneButton);
        ajouterLigne.setOnTouchListener(eventListener.getOnAjouterLigneListener());
        tableLigne = new TableLayout(getActivity());

        int size = data.getData().getPartition().getLen();

        for(Map.Entry<Integer, List<Vector2>> entry : data.getData().getPartition().getPartitionData().entrySet()) {
            PartitionLineView line = addLineView(entry.getKey(), size);
            if (entry.getValue() != null) {
                for (int j = 0; j < entry.getValue().size(); j++) {
                    Vector2 vec = entry.getValue().get(j);
                    NoteView note = new NoteView(getActivity(), line);
                    note.setPosX(vec.getX());
                    note.setSize(vec.getY());
                    line.addView(note);
                    note.setOnTouchListener(eventListener.getOnTouchNoteListener());
                    note.setLongClickable(true);
                    note.setOnLongClickListener(eventListener.getLongNoteClickListener());
                }
            }
        }

        scrollView = (ScrollView)getActivity().findViewById(R.id.PartPageScrollPart);
        scrollView.removeAllViews();
        scrollView.addView(tableLigne);
        ajouterLigne.setOnTouchListener(eventListener.getOnAjouterLigneListener());

        if(horizontalScroll == null) {
            horizontalScroll = new PartitionBottomScroll(getActivity(), tableLigne, size);
        }

        GridLayout p = (GridLayout)horizontalScroll.getParent();
        if(p != null)p.removeView(horizontalScroll);

        ((GridLayout)getActivity().findViewById(R.id.PartPageBottomScroll)).addView(horizontalScroll);


        longueurView = (EditText)getActivity().findViewById(R.id.LongueurNumber);
        positionView = (EditText)getActivity().findViewById(R.id.PositionNumber);
        totalView = (EditText)getActivity().findViewById(R.id.TotalNumber);

        longueurView.setEnabled(false);
        positionView.setEnabled(false);
        totalView.setText(Integer.toString(size));
        totalView.addTextChangedListener(eventListener.getTextTotalChangeListener());
        totalView.setEnabled(true);
        deselectLine();
    }

    public void addLine(int id) {
        if(id != data.getId()) {
            data.getData().getPartition().addLine(id);
        }
    }

    private PartitionLineView addLineView(int id, int len) {
        scrollView = (ScrollView)getActivity().findViewById(R.id.PartPageScrollPart);
        scrollView.removeAllViews();
        scrollView.addView(tableLigne);
        System.out.println(tableLigne.getChildCount());
        PartitionLineView lineView = new PartitionLineView(getActivity(), id, len);
        lineView.setOnTouchListener(eventListener.getOnLineTouchListener());
        lineView.setOnDragListener(eventListener.getOnDragListener());
        tableLigne.addView(lineView);
        scrollView.invalidate();
        return  lineView;
    }


    public PartitionBin getPartitionBin() {
        return partitionBin;
    }

    public void setOnEdit(NoteView v, boolean b) {
        if (selection != null) {
            selection.setOnEdit(false);
        }

        if (b) {
            selection = v;
            v.setOnEdit(b);
            selection.getFather().removeView(selection);
            selection.getFather().addView(selection); //on le met en premier plan


            //On met les informations dans les EditView
            longueurView.setText(Integer.toString(v.getRealSize()));
            positionView.setText(Integer.toString(v.getRealPosX() - PartitionLineView.BEGIN_LINE));

        } else {

            longueurView.setText("");
            positionView.setText("");
            selection = null;
        }
    }

    public void lineChange(PartitionLineView line) throws ParserConfigurationException {
        int id = line.getNoteId();
        List<Vector2> l_data = data.getData().getPartition().getLine(id);
        if(l_data != null) { //impossible
            l_data.clear();
            for(int i = 0; i < line.getChildCount(); i++) {
                NoteView child = (NoteView)line.getChildAt(i);
                l_data.add(new Vector2(child.getPosX(), child.getSize()));
            }
        }

        if (selection != null) {
            longueurView.setText(Integer.toString(selection.getRealSize()));
            positionView.setText(Integer.toString(selection.getRealPosX() - PartitionLineView.BEGIN_LINE));
        }

        save();

    }

    public NoteView getSelection() {
        return selection;
    }

    public void setSelectionLength(int selectionLength) {
        if(selection != null) {
            selection.setSizeWithCollisionVerif(selectionLength);
        }
    }


    public void setSelectionPosition(int selectionPosition) {
        if(selection != null) {
            selection.setPosXWithCollisionVerif(selectionPosition);
        }
    }


    public void changeLinesLength(int len) throws ParserConfigurationException {
        if(len == 0) {
            totalView.setText("10");
            totalView.setSelection(2);
            return;
        }
        List<PartitionLineView> lines = new ArrayList<>();
        data.getData().getPartition().setLen(len);
        for(int i = 0 ; i < tableLigne.getChildCount(); i++) {
            PartitionLineView line = (PartitionLineView)tableLigne.getChildAt(i);
            PartitionLineView aux = new PartitionLineView(getActivity(), line.getNoteId(), len);
            aux.setOnTouchListener(eventListener.getOnLineTouchListener());
            aux.setOnDragListener(eventListener.getOnDragListener());
            lines.add(aux);
            List<NoteView> notes = new ArrayList<>();
            for(int j = 0; j < line.getChildCount(); j++) {
                NoteView v = (NoteView)line.getChildAt(j);
                if(v.getRealPosX() + v.getRealSize() > len + line.BEGIN_LINE) {

                    if (v.getPosX() < len) {
                        v.setSize((len  + line.BEGIN_LINE) - v.getRealPosX() + NoteResizer.WIDTH);
                        notes.add(v);
                    }
                } else {
                    notes.add(v);
                }

            }
            line.removeAllViews();
            for(NoteView v : notes) {
                v.setFather(aux);
                aux.addView(v);
            }
        }

        tableLigne.removeAllViews();
        for(PartitionLineView line : lines) {
            tableLigne.addView(line);
            lineChange(line);
        }

        horizontalScroll.resize(len);
    }

    public void setPartitionBin(PartitionBin partitionBin) {
        this.partitionBin = partitionBin;
    }


    public void scrollLine(int pos) {
        for(int i = 0; i < tableLigne.getChildCount(); i++) {
            PartitionLineView line = (PartitionLineView)tableLigne.getChildAt(i);
            line.setScrollBegin(pos);
            line.requestLayout();
        }
    }

    public void removeLine() throws ParserConfigurationException {
        if(lineSelected != null) {
            int id = lineSelected.getNoteId();
            data.getData().getPartition().removeLine(id);
            setData(data);
            save();
            deselectLine();
        }
    }

    public void setLineSelected(PartitionLineView line) {
        if(lineSelected != null) {
            lineSelected.setOnShadow(false);
        }


        lineSelected = line;
        lineSelected.setOnShadow(true);
        Button b = ((Button)getActivity().findViewById(R.id.SupprLigneButton));
        b.setOnTouchListener(eventListener.getOnSupprLineListener());
        getActivity().findViewById(R.id.SupprLigneButton).setVisibility(View.VISIBLE);

    }

    public void deselectLine() {
        if(lineSelected != null) {
            lineSelected.setOnShadow(false);
        }
        lineSelected = null;
        getActivity().findViewById(R.id.SupprLigneButton).setVisibility(View.INVISIBLE);
    }


}
