package univ.etu.soundroid.Controller;

import android.widget.Button;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.activity.PartitionActivity;
import univ.etu.soundroid.View.Partition.Event.PartitionEventListener;

/**
 * Created by emile on 28/11/2015.
 */
public class PartitionController extends Controller {

    private static volatile PartitionController instance = null;
    private PartitionEventListener eventListener = null;
    private Button buttonPlus = null;
    private Button buttonPlay = null;

    public static PartitionController getInstance() {
        if(instance == null) {
            instance = new PartitionController();
        }
        return instance;
    }

    public void set(PartitionActivity activity) {
        super.setActivity(activity);
        eventListener = PartitionEventListener.getInstance();

        buttonPlay = (Button) activity.findViewById(R.id.butPlay);
        buttonPlus = (Button) activity.findViewById(R.id.butPlus);

        buttonPlus.setOnTouchListener(eventListener.getOnAddNoteListener());
        buttonPlay.setOnTouchListener(eventListener.getOnPlayListener());

    }


    public void addTab(int id) {
        ((PartitionActivity)getActivity()).addTab(id);
    }



    public PartitionEventListener getEventListener() {
        return eventListener;
    }

    public void removeTab(int note) {
        ((PartitionActivity)getActivity()).removeTab(note);
    }

    public boolean isOpen(int noteId) {
        return ((PartitionActivity)getActivity()).isOpen(noteId);
    }
}
