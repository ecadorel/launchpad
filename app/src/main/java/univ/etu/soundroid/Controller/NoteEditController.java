package univ.etu.soundroid.Controller;

import android.text.InputType;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.Note;
import univ.etu.soundroid.Models.Sound;
import univ.etu.soundroid.Models.SoundManager;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.view.PartitionBin;
import univ.etu.soundroid.View.Partition.activity.NoteEditActivity;
import univ.etu.soundroid.View.Partition.Event.NoteEditEventListener;

/**
 * Created by emile on 28/11/2015.
 */
public class NoteEditController extends Controller {

    private static volatile NoteEditController instance = null;
    private NoteEditEventListener eventListener = null;
    private Button buttonInformation = null;
    private Button buttonFermerOnglet = null;
    private Button buttonPartition = null;
    private CheckBox checkispartition = null;
    private SurfaceView color = null;
    private TextView sonLabel = null;
    private EditText sonPath = null;
    private Button sonFileOpener = null;
    private PartitionBin partitionBin = null;
    private Note data = null;
    private String test;

    public static NoteEditController getInstance() {
        if(instance == null) {
            instance = new NoteEditController();
        }
        return instance;
    }

    public void set(NoteEditActivity activity) {
        super.setActivity(activity);
        eventListener = NoteEditEventListener.getInstance();

    }


    public void onInformation() throws ParserConfigurationException {
        if(data == null) {
            return;
        }
        getActivity().findViewById(R.id.AjouterLigneButton).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.LongueurNumber).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.TotalNumber).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.PositionNumber).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.PositionLabel).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.LongueurLabel).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.TotalLabel).setVisibility(View.INVISIBLE);
        getActivity().findViewById(R.id.SupprLigneButton).setVisibility(View.INVISIBLE);

        buttonPartition = (Button) getActivity().findViewById(R.id.buttPartition);
        buttonInformation = (Button) getActivity().findViewById(R.id.butInformation);
        buttonFermerOnglet = (Button) getActivity().findViewById(R.id.buttCloseTab);

        buttonInformation.setOnTouchListener(eventListener.getOnInformationListener());
        buttonPartition.setOnTouchListener(eventListener.getOnPartitionListener());
        buttonFermerOnglet.setOnTouchListener(eventListener.getOnCloseTabListener());

        checkispartition = (CheckBox)getActivity().findViewById(R.id.NoteEditispartition);
        if(checkispartition != null) {
            checkispartition.setChecked(data.getData().isPartition());
            checkispartition.setOnCheckedChangeListener(eventListener.getOnIsPartitionChangeListener());

            color = (SurfaceView) getActivity().findViewById(R.id.NoteEditAColor);
            color.setBackgroundColor(data.getData().getColor());
            color.setOnTouchListener(eventListener.getOnColorListener());
            color.invalidate();

            sonLabel = (TextView) getActivity().findViewById(R.id.NoteEditSonLabel);
            sonPath = (EditText) getActivity().findViewById(R.id.NoteEditSonPath);
            Sound s = SoundManager.getInstance().getSound(data.getData().getSoundId());

            if (s != null) {
                if (s.getPath() != null) {
                    sonPath.setText(s.getPath());
                } else {
                    sonPath.setText(getActivity().getResources().getString(s.getIdResource()));
                }
            }
            sonPath.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            sonPath.setEnabled(false);
            sonPath.invalidate();

            sonFileOpener = (Button) getActivity().findViewById(R.id.NoteEditFileButton);
            sonFileOpener.setOnTouchListener(eventListener.getOnFileOpenerListener());
            activatePartition(data.getData().isPartition());
        } else {
            getActivity().finish();
        }
    }

    public void activatePartition(boolean active) throws ParserConfigurationException {
        if(data == null) {
            return;
        }
        sonLabel.setEnabled(!active);
        sonFileOpener.setEnabled(!active);
        buttonPartition.setEnabled(active);
        data.getData().setIsPartition(active);

        save();
    }

    public void setSoundId(int soundId) {
        data.getData().setSoundId(soundId);
    }

    public NoteEditEventListener getEventListener() {
        return eventListener;
    }

    public void setData(Note data) {
        this.data = data;
    }

    public void setId(int id) {
        data = GlobalData.getInstance().getGrid().getNote(id);
    }

    public void setNoteColor(int noteColor) throws ParserConfigurationException {
        data.getData().setColor(noteColor);
        save();
    }

    public int getNoteColor() {
        return data.getData().getColor();
    }


    public void onPartition() {
        getActivity().findViewById(R.id.AjouterLigneButton).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.LongueurNumber).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.TotalNumber).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.PositionNumber).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.PositionLabel).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.LongueurLabel).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.TotalLabel).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.SupprLigneButton).setVisibility(View.INVISIBLE);

        if(partitionBin == null) {
            partitionBin = new PartitionBin(getActivity());
        }

        GridLayout p = (GridLayout)partitionBin.getParent();
        if(p != null) p.removeView(partitionBin);

        PartitionPageController.getInstance().setPartitionBin(partitionBin);
        ((GridLayout) getActivity().findViewById(R.id.PartPageInfoBlock)).addView(partitionBin,
                new GridLayout.LayoutParams(GridLayout.spec(4, GridLayout.CENTER), GridLayout.spec(1, GridLayout.CENTER)));

        partitionBin.setVisibility(View.INVISIBLE);

        getActivity().findViewById(R.id.LongueurNumber).setEnabled(false);
        getActivity().findViewById(R.id.TotalNumber).setEnabled(false);
        getActivity().findViewById(R.id.PositionNumber).setEnabled(false);

        PartitionPageController.getInstance().set((NoteEditActivity) getActivity());
        PartitionPageController.getInstance().setData(data);
        ((GridLayout)getActivity().findViewById(R.id.PartPageInfoBlock)).invalidate();
    }

    public int getNoteId() {
        return data.getId();
    }
}
