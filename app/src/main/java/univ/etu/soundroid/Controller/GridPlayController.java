package univ.etu.soundroid.Controller;

import android.widget.FrameLayout;

import java.util.Map;

import univ.etu.soundroid.Models.Conductor;
import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.Note;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.GridCommon.view.GridView;
import univ.etu.soundroid.View.GridCommon.view.NoteView;
import univ.etu.soundroid.View.GridPlay.activity.GridPlayActivity;
import univ.etu.soundroid.View.GridPlay.event.GridPlayEventListener;

/**
 * Created by emile on 29/11/2015.
 */
public class GridPlayController extends Controller {

    private static volatile GridPlayController instance = null;
    private GridPlayEventListener eventListener = null;
    private GridView m_grid = null;
    private FrameLayout globalLayout;
    private Conductor conductor = null;


    public static GridPlayController getInstance() {
        if(instance == null) {
            instance = new GridPlayController();
        }
        return instance;
    }


    public void set(GridPlayActivity activity) {
        super.setActivity(activity);
        eventListener = GridPlayEventListener.getInstance();

        globalLayout = (FrameLayout) activity.findViewById(R.id.gridPlayLayout);
        m_grid = new GridView(getActivity());
        generateGrid();
        globalLayout.addView(m_grid);
    }

    private void generateGrid() {

        for(Map.Entry<Integer, Note> entry : GlobalData.getInstance().getGrid().getEntrySet())  {
            NoteView note = new NoteView(getActivity());
            note.setId(entry.getKey());
            note.setText(entry.getKey().toString());
            note.setTouchColor(entry.getValue().getData().getColor());
            note.setOnTouchListener(eventListener.getOnNoteTouchListener());
            m_grid.addView(note);
        }

    }

    public void setButtonPressed(boolean b, int color, int id) {
        for(int i = 0; i < m_grid.getChildCount(); i++) {
            if(((NoteView)m_grid.getChildAt(i)).getId() == id) {
                ((NoteView) m_grid.getChildAt(i)).setTouchColor(color);
                ((NoteView) m_grid.getChildAt(i)).setIsTouch(b);
                ((NoteView) m_grid.getChildAt(i)).postInvalidate();
            }
        }
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void onPause() {
        conductor.stopThread();
        try {
            conductor.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onResume() {
        conductor = new Conductor();
        conductor.start();
        conductor.reset();
    }

    public void onFinish() {
        conductor.stopThread();
        try {
            conductor.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
