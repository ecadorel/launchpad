package univ.etu.soundroid.Controller;

import android.widget.FrameLayout;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.Note;
import univ.etu.soundroid.Models.SoundManager;
import univ.etu.soundroid.Models.Vector2;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.GridConfig.activity.GridConfigActivity;
import univ.etu.soundroid.View.GridConfig.event.GridConfigEventListener;
import univ.etu.soundroid.View.GridCommon.view.GridView;
import univ.etu.soundroid.View.GridCommon.view.NoteView;

/**
 * Created by emile on 28/11/2015.
 */
public class GridConfigController extends Controller {

    private GridConfigState mode;
    private int fromNoteId = -1;

    public enum GridConfigState implements Serializable {
        LINE, NOTE
    }


    private static volatile GridConfigController instance = null;
    private GridConfigEventListener eventListener = null;


    private FrameLayout globalLayout = null;
    private GridView m_grid = null;

    public static GridConfigController getInstance() {
        if(instance == null) {
            instance = new GridConfigController();
        }
        return instance;
    }

    public void set(GridConfigActivity activity) {
        super.setActivity(activity);
        eventListener = GridConfigEventListener.getInstance();
        this.mode = (GridConfigState)getActivity().getIntent().getSerializableExtra("mode");
        if(this.mode == GridConfigState.LINE) {
            this.fromNoteId = (int)getActivity().getIntent().getSerializableExtra("noteId");
        }

        globalLayout = (FrameLayout) activity.findViewById(R.id.gridConfigLayout);
        m_grid = new GridView(getActivity());
        generateGrid();
        globalLayout.addView(m_grid);
    }

    private void generateGrid() {

        m_grid.removeAllViews();
        for(Map.Entry<Integer, Note> entry : GlobalData.getInstance().getGrid().getEntrySet())  {
            NoteView note = new NoteView(getActivity());
            note.setId(entry.getKey());
            note.setBasicColor(entry.getValue().getData().getColor());
            note.setText(entry.getKey().toString());

            if(mode == GridConfigState.NOTE) {
                note.setDeletable(true);
                note.getDeleter().setOnTouchListener(eventListener.getOnDeleteNoteListener());
                note.setOnTouchListener(eventListener.getOnNoteTouchListener());
            } else {
                if(isValidNote(note)) {
                    note.setOnTouchListener(eventListener.getOnNoteForLineTouchListener());
                } else {
                    note.setIsActive(false);
                }
            }
            m_grid.addView(note);
        }

        if(mode == GridConfigState.NOTE) {
            NoteView note = new NoteView(getActivity());
            note.setText("+");
            note.setOnTouchListener(eventListener.getOnAddTouchListener());
            m_grid.addView(note);
        }
        m_grid.invalidate();
    }

    private boolean isValidNote(NoteView note) {
        if(note.getId() == fromNoteId) {
            return false;
        } else {
            Note n = GlobalData.getInstance().getGrid().getNote(note.getId());
            if(n.getData().isPartition()) {
                for (Map.Entry<Integer, List<Vector2>> entry : n.getData().getPartition().getPartitionData().entrySet()) {
                    if (entry.getKey().equals(fromNoteId)) {
                        return false;
                    }
                }
            }

            n = GlobalData.getInstance().getGrid().getNote(fromNoteId);
            if(n.getData().isPartition()) {
                for (Map.Entry<Integer, List<Vector2>> entry : n.getData().getPartition().getPartitionData().entrySet()) {
                    if (entry.getKey().equals(note.getId())) {
                        return false;
                    }
                }
            }

        }
        return true;
    }

    public int addNote() throws ParserConfigurationException {
        Note note = GlobalData.getInstance().getGrid().addNote();
        if(note.getId() % 2 == 0) {
            note.getData().setColor(getActivity().getResources().getColor(R.color.pale_green));
        } else {
            note.getData().setColor(getActivity().getResources().getColor(R.color.pale_blue));
        }
        note.getData().setIsPartition(false);
        note.getData().setSoundId(SoundManager.getInstance().addSound(R.raw.son1));
        save();
        return note.getId();
    }

    public void removeNote(int note) throws ParserConfigurationException {
        GlobalData.getInstance().getGrid().removeNote(note);
        PartitionController.getInstance().removeTab(note);
        save();
        generateGrid();
    }


    public GridConfigEventListener getEventListener() {
        return eventListener;
    }

}
