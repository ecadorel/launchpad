package univ.etu.soundroid.Controller;

import android.app.Activity;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Models.FileTool;
import univ.etu.soundroid.Models.Sound;
import univ.etu.soundroid.Models.SoundManager;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.FileDialog.FileDialog;
import univ.etu.soundroid.View.Partition.Event.SoundChoiceEventListener;
import univ.etu.soundroid.View.Partition.activity.SoundChoiceActivity;

/**
 * Created by Guillaume on 20/12/2015.
 */
public class SoundChoiceController extends Controller {
    public static final String BASIC_PATH = FileTool.SdCard.getAbsolutePath();
    private static volatile SoundChoiceController instance = null;

    private List<Sound> soundList = null;
    private FileDialog chargerDialog;

    public static SoundChoiceController getInstance() {
        if(instance == null) {
            instance = new SoundChoiceController();
        }
        return instance;
    }

    public void set(Activity activity) {
        super.setActivity(activity);
        SoundChoiceEventListener soundEventListener = SoundChoiceEventListener.getInstance();

        Button fileLoader = (Button)activity.findViewById(R.id.buttonLoadFile);
        fileLoader.setOnTouchListener(soundEventListener.getOnLoadFileListener());

        ListView listView = (ListView)activity.findViewById(R.id.listView);

        soundList = SoundManager.getInstance().getSoundList();
        List<HashMap<String,String>> soundListMap = new ArrayList<>();

        HashMap<String, String> elem;
        ListIterator<Sound> it = soundList.listIterator();
        int i = 0;
        while(it.hasNext()) {
            elem = new HashMap<>();
            Sound sound = it.next();
            if(sound.getPath() != null && sound.getPath().length() > 0) {
                elem.put("SoundPath", sound.getPath());
            } else {
                elem.put("SoundPath", activity.getResources().getString(sound.getIdResource()));
            }
            elem.put("SoundIndex", String.valueOf(i));
            soundListMap.add(elem);
            i++;
        }

        ListAdapter adapter = new SimpleAdapter(activity, soundListMap, android.R.layout.simple_list_item_1, new String[] {"SoundPath"}, new int[] {android.R.id.text1});
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(soundEventListener.getOnItemClickListener());
    }

    public void setSelectedSound(int position) {
        NoteEditController.getInstance().setSoundId(position);
        getActivity().finish();
    }

    public void addSound(String path) throws ParserConfigurationException {
        //setSelectedSound(SoundManager.getInstance().addSound(path));
        SoundManager.getInstance().addSound(path);
        set(getActivity());
        save();
    }

    public void setChargerDialog(FileDialog chargerDialog) {
        this.chargerDialog = chargerDialog;
    }

    public FileDialog getChargerDialog() {
        return chargerDialog;
    }
}
