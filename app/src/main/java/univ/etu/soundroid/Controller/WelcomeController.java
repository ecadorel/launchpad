package univ.etu.soundroid.Controller;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Models.FileTool;
import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.IO.DocumentMalformedException;
import univ.etu.soundroid.Models.Sound;
import univ.etu.soundroid.Models.SoundroidAppData;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.FileDialog.FileDialog;
import univ.etu.soundroid.View.Partition.activity.PartitionActivity;
import univ.etu.soundroid.View.Welcome.activity.WelcomeActivity;
import univ.etu.soundroid.View.Welcome.dialog.AProposDialog;
import univ.etu.soundroid.View.Welcome.dialog.WelcomeDialog;
import univ.etu.soundroid.View.Welcome.event.WelcomeEventListener;

/**
 * Created by emile on 24/12/2015.
 */
public class WelcomeController extends Controller {

    private static volatile WelcomeController instance = null;
    public static final String BASIC_PATH = FileTool.SdCard.getAbsolutePath() + "/Documents/SoundroidProjects/";
    private Button nouveau = null;
    private Button apropos = null;
    private Button charger = null;
    private WelcomeEventListener eventListener = null;
    private WelcomeDialog dialog = null;
    private AProposDialog appdialog = null;
    private FileDialog chargerDialog;

    public static WelcomeController getInstance() {
        if(instance == null) {
            instance = new WelcomeController();
        }
        return instance;
    }

    public void set(WelcomeActivity activity) {
        super.setActivity(activity);
        eventListener = WelcomeEventListener.getInstance();
        nouveau = (Button)getActivity().findViewById(R.id.buttNouveau);
        nouveau.setOnClickListener(eventListener.getOnNouveau());

        charger = (Button)getActivity().findViewById(R.id.buttCharger);
        charger.setOnClickListener(eventListener.getOnCharger());

        apropos = (Button)getActivity().findViewById(R.id.buttPropos);
        apropos.setOnClickListener(eventListener.getOnPropos());

        loadRecentProject();

    }

    private void loadRecentProject() {
        //TODO charger dans le fichier xml
        List<HashMap<String, String>> projectList = new ArrayList<>();
        ListView listView = (ListView)getActivity().findViewById(R.id.welcomeTableProjet);
        HashMap<String, String> elem;

        for(int i = 0 ; i < 10 && i < SoundroidAppData.getRecentProject().size(); i++) {
            elem = new HashMap<>();
            elem.put("ProjectName", SoundroidAppData.getRecentProject().get(i).getName());
            elem.put("ProjectFile", SoundroidAppData.getRecentProject().get(i).getPath());
            projectList.add(elem);
        }

        ListAdapter adapter = new SimpleAdapter(getActivity(), projectList, android.R.layout.simple_list_item_1, new String[] {"ProjectName"}, new int[] {android.R.id.text1});
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(eventListener.getOnItemClickListener());
    }

    public void openProject(String name, boolean isNew) {

        if(isNew){//si le fichier est un nouveau projet, on vérifie qu'il n'existe pas déja
            File file = new File(GlobalData.getInstance().BASIC_PATH + name + ".xml");
            if(file.exists()){//si c'est le cas
                int cpt =0;
                do{
                    cpt++;
                    file = new File(GlobalData.getInstance().BASIC_PATH + name + "_" + cpt + ".xml"); //on ajoute un _X au nom du fichier
                }while(file.exists());
                name = name + "_" + cpt;
            }
        }
        GlobalData.getInstance().setDefaultProjectFile(name);

        if(!isNew) {
            try {
                open(GlobalData.getInstance().getProjectFile().getPath());
            } catch (ParserConfigurationException | DocumentMalformedException | IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }else{
            GlobalData.getInstance().clear();
        }

        Intent intent = new Intent(WelcomeController.getInstance().getActivity(), PartitionActivity.class);
        WelcomeController.getInstance().getActivity().startActivity(intent);
    }

    public void openProjectWithPath(String path) {

        GlobalData.getInstance().setProjectFile(path);

        try {
            open(path);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (DocumentMalformedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(WelcomeController.getInstance().getActivity(), PartitionActivity.class);
        WelcomeController.getInstance().getActivity().startActivity(intent);
    }

    public void setDialog(WelcomeDialog dialog) {
        this.dialog = dialog;
    }

    public WelcomeDialog getDialog() {
        return dialog;
    }

    public AProposDialog getAppdialog() {
        return appdialog;
    }

    public void setAppdialog(AProposDialog appdialog) {
        this.appdialog = appdialog;
    }

    public void setChargerDialog(FileDialog chargerDialog) {
        this.chargerDialog = chargerDialog;
    }

    public FileDialog getChargerDialog() {
        return chargerDialog;
    }
}
