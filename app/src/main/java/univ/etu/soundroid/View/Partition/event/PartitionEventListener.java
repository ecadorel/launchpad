package univ.etu.soundroid.View.Partition.Event;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;

import univ.etu.soundroid.Controller.GridConfigController;
import univ.etu.soundroid.Controller.PartitionController;
import univ.etu.soundroid.View.GridConfig.activity.GridConfigActivity;
import univ.etu.soundroid.View.GridPlay.activity.GridPlayActivity;

/**
 * Created by emile on 28/11/2015.
 */
public class PartitionEventListener {

    private static volatile PartitionEventListener instance = null;
    private View.OnTouchListener onAddNoteListener = null;
    private View.OnTouchListener onPlayListener = null;

    public static PartitionEventListener getInstance() {
        if(instance == null) {
            instance = new PartitionEventListener();
        }
        return instance;
    }

    public PartitionEventListener() {

        onAddNoteListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(PartitionController.getInstance().getActivity(), GridConfigActivity.class);
                    intent.putExtra("mode", GridConfigController.GridConfigState.NOTE);
                    PartitionController.getInstance().getActivity().startActivity(intent);
                }
                return false;
            }
        };


        onPlayListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(PartitionController.getInstance().getActivity(), GridPlayActivity.class);
                    PartitionController.getInstance().getActivity().startActivity(intent);
                }
                return false;
            }
        };
    }

    public View.OnTouchListener getOnAddNoteListener() {
        return onAddNoteListener;
    }

    public View.OnTouchListener getOnPlayListener() {
        return onPlayListener;
    }
}
