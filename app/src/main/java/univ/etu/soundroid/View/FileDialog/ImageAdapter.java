package univ.etu.soundroid.View.FileDialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import univ.etu.soundroid.R;

/**
 * Created by emile on 30/12/2015.
 */
public class ImageAdapter extends ArrayAdapter {
    Activity context;
    List<Map<String, String>> items;
    List<Integer> arrows;
    int layoutId;
    int textId;
    int imageId;
    String displayString;

    ImageAdapter(Activity context, int layoutId, int textId, int imageId, List<Map<String, String>> items, String display, List<Integer> arrows) {
        super(context, layoutId, items);

        this.context = context;
        this.items = items;
        this.arrows = arrows;
        this.displayString = display;
        this.layoutId = layoutId;
        this.textId = textId;
        this.imageId = imageId;
    }

    public View getView(int pos, View convertView, ViewGroup parent)  {

        LayoutInflater inflater=context.getLayoutInflater();
        View row=inflater.inflate(layoutId, null);
        TextView label=(TextView)row.findViewById(textId);

        label.setText(items.get(pos).get(displayString));

        if (arrows.get(pos) == 0) {
            ImageView icon=(ImageView)row.findViewById(imageId);
            icon.setImageResource(R.drawable.arrow0);
        } else if (arrows.get(pos) == 1) {
            ImageView icon = (ImageView) row.findViewById(imageId);
            icon.setImageResource(R.drawable.arrow1);
        } else if (arrows.get(pos) == 2) {
            ImageView icon = (ImageView) row.findViewById(imageId);
            icon.setImageResource(R.drawable.arrow2);
        }

        return(row);
    }
}

