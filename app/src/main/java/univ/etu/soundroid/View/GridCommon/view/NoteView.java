package univ.etu.soundroid.View.GridCommon.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import univ.etu.soundroid.Controller.GridConfigController;
import univ.etu.soundroid.R;

/**
 * Created by emile on 28/11/2015.
 */
public class NoteView extends ViewGroup {

    private int posX;
    private int posY;
    private String text;
    private int id;

    private int touchColor;
    private int basicColor;
    private int hiddenColor;

    private boolean isTouch = false;
    private boolean is_active = true;

    private int width = 100
             , height = 100;
    private NoteDeleter child = null;
    private boolean is_deletable = false;

    public NoteView(Context context) {
        super(context);
        hiddenColor = super.getContext().getResources().getColor(R.color.broken_white);
        touchColor = super.getContext().getResources().getColor(R.color.blue);
        basicColor = super.getContext().getResources().getColor(R.color.materialcolorpicker__lightgrey);
        child = new NoteDeleter(context);
        addView(child);
        child.setVisibility(INVISIBLE);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();
        for(int i = 0; i < count; i++) {
            View v = getChildAt(i);
            if(v.getVisibility() != GONE) {
                v.layout(0, 0, 40, 40);
            }
        }
        invalidate();
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        Paint color = new Paint();
        if(is_active) {
            if (isTouch) {
                color.setColor(touchColor);
            } else {
                color.setColor(basicColor);
            }
        } else {
            color.setColor(hiddenColor);
        }

        canvas.drawRect(1, 1, width - 1, height - 1, color);
        Paint black = new Paint();
        Rect bounds = new Rect();
        black.setTextSize(20);
        black.getTextBounds(text, 0, text.length(), bounds);
        canvas.drawText(text, width / 2 - bounds.width() / 2, height / 2 - bounds.height() / 2, black);


        super.dispatchDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.setMeasuredDimension(width, height);
        requestLayout();
    }


    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setIsTouch(boolean isTouch) {
        this.isTouch = isTouch;
    }

    public void setIsActive(boolean isActive) {
        this.is_active = isActive;
    }

    public boolean isActive() {
        return is_active;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public void setSize(int size) {
        this.height = size;
        this.width = size;
    }

    public void setTouchColor(int color) {
        this.touchColor = color;
    }

    public void setBasicColor(int color) {
        this.basicColor = color;
    }

    public void setHiddenColor(int color) {
        this.hiddenColor = color;
    }

    public void setDeletable(boolean is_deletable) {
        this.is_deletable = is_deletable;
        if(is_deletable) {
            child.setVisibility(VISIBLE);
        } else {
            child.setVisibility(INVISIBLE);
        }

    }

    public NoteDeleter getDeleter() {
        return child;
    }


}
