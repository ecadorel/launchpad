package univ.etu.soundroid.View.Partition.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;

import univ.etu.soundroid.Controller.PartitionPageController;
import univ.etu.soundroid.R;

/**
 * Created by emile on 22/12/2015.
 */
public class PartitionBottomScroll extends ViewGroup {

    //l'objet qui contient les elements a scroller
    public ViewGroup container = null;
    public Scroll child;

    private static final int BASIC_BIAS = 25;
    private int width;
    private int height;
    private int scrollSize;

    public PartitionBottomScroll(Context context, ViewGroup group, int size) {
        super(context);
        container = group;
        scrollSize = size;
    }

    public void resize(int size) {
        scrollSize = size;
        int aux = (int)(((float)width / (float)size) * width) - BASIC_BIAS;
        System.out.println("SIZE :" + aux);
        if(width < size) {
            child.setSize(aux);
        } else {
            child.setSize(width - BASIC_BIAS);
            child.resetPosition();
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        Paint back = new Paint();
        back.setColor(super.getContext().getResources().getColor(R.color.materialcolorpicker__lightgrey));

        //canvas.drawRect(0, 0, width, height, back);
        super.dispatchDraw(canvas);
    }

    public boolean requestMove(int position, int size) {
        if (position > 0 && position + size < width) {
            return true;
        }
        return false;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();
        for(int i = 0; i < count; i++) {
            final Scroll child = (Scroll)getChildAt(i);
            if (child.getVisibility() != View.GONE) {
                child.layout(child.getPos(), 4, child.getSize() + child.getPos(), getHeight() - 4);
            }
        }

        //Calcule la position relative du scroll sur la barre de scroll et avertis toutes les lineView le changement de position
/*        int percent = child.getPos() / width * scrollSize;
        for (int i = 0; i < container.getChildCount(); i++) {
            PartitionLineView line = (PartitionLineView)container.getChildAt(i);
            line.setBeginDraw(percent);
        }
*/
        invalidate();
    }

    public void addView(Scroll v) {
        removeAllViews();
        super.addView(v);
        child = v;
        child.setOnTouchListener(PartitionPageController.getInstance().getEventListener().onScrollTouchListener());
    }

    @Override
    public void addView(View v) {}

    @Override
    public void onMeasure(int widthSpec, int heightSpec) {
        width = MeasureSpec.getSize(widthSpec);
        height = MeasureSpec.getSize(heightSpec);
        if(child == null) {
            if (width > 0) {
                if (scrollSize > width) {
                    int aux = (int) (((float) width / (float) scrollSize) * width) - BASIC_BIAS;
                    child = new Scroll(getContext(), aux);
                } else {
                    child = new Scroll(getContext(), width - BASIC_BIAS);
                }
            } else {
                child = new Scroll(getContext(), 0);
            }

            addView(child);
        }

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != View.GONE) {
                measureChild(child, widthSpec, heightSpec);
            }
        }

        super.setMeasuredDimension(width, height);
    }

    public int getScrollSize() {
        return scrollSize;
    }


}
