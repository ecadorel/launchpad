package univ.etu.soundroid.View.Partition.Event;

import android.content.Intent;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Controller.NoteEditController;
import univ.etu.soundroid.Controller.PartitionController;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.activity.NoteEditActivity;
import univ.etu.soundroid.View.Partition.activity.SoundChoiceActivity;

/**
 * Created by emile on 28/11/2015.
 */
public class NoteEditEventListener {

    private static volatile NoteEditEventListener instance = null;
    private View.OnTouchListener onInformationListener = null;
    private View.OnTouchListener onPartitionListener;
    private CompoundButton.OnCheckedChangeListener onIsPartitionChangeListener;
    private View.OnTouchListener onColorListener;
    private View.OnTouchListener onFileOpenerListener;
    private View.OnTouchListener onCloseTabListener;


    public static NoteEditEventListener getInstance() {
        if(instance == null) {
            instance = new NoteEditEventListener();
        }
        return instance;
    }


    private NoteEditEventListener() {

        onColorListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    final SurfaceView view = (SurfaceView) v;
                    int col = NoteEditController.getInstance().getNoteColor();
                    Color color = new Color();
                    int red = color.red(col);
                    int green = color.green(col);
                    int blue = color.blue(col);
                    final ColorPicker cp = new ColorPicker(NoteEditController.getInstance().getActivity(), red, green, blue);
                    cp.show();
                    Button okColor = (Button) cp.findViewById(R.id.okColorButton);

                    okColor.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            view.setBackgroundColor(cp.getColor());
                            try {
                                NoteEditController.getInstance().setNoteColor(cp.getColor());
                            } catch (ParserConfigurationException e) {
                                e.printStackTrace();
                            }
                            cp.dismiss();
                        }
                    });
                }
                return false;
            }
        };

        onIsPartitionChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    //On cache les autres elements
                    try {
                        NoteEditController.getInstance().activatePartition(true);
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        NoteEditController.getInstance().activatePartition(false);
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        onInformationListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        ((NoteEditActivity)NoteEditController.getInstance().getActivity()).setOnInformation();
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        };

        onPartitionListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    ((NoteEditActivity)NoteEditController.getInstance().getActivity()).setOnPartition();
                }

                return false;
            }
        };

        onCloseTabListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    PartitionController.getInstance().removeTab(NoteEditController.getInstance().getNoteId());
                }

                return false;
            }
        };


        onFileOpenerListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(NoteEditController.getInstance().getActivity(), SoundChoiceActivity.class);
                    NoteEditController.getInstance().getActivity().startActivity(intent);
                }
                return false;
            }
        };

    }


    public View.OnTouchListener getOnInformationListener() {
        return onInformationListener;
    }

    public View.OnTouchListener getOnPartitionListener() {
        return onPartitionListener;
    }

    public CompoundButton.OnCheckedChangeListener getOnIsPartitionChangeListener() {
        return onIsPartitionChangeListener;
    }

    public View.OnTouchListener getOnColorListener() {
        return onColorListener;
    }

    public View.OnTouchListener getOnFileOpenerListener() {
        return onFileOpenerListener;
    }

    public View.OnTouchListener getOnCloseTabListener() {
        return onCloseTabListener;
    }
}
