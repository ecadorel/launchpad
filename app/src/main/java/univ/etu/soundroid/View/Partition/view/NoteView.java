package univ.etu.soundroid.View.Partition.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.Event.PartitionPageEventListener;

public class NoteView extends ViewGroup {
    /*La taille de base d'une case quand on l'ajoute en cliquant*/

    private static final int BASIC_SIZE = 70;
    private static final int SPACE = 2;
    private int m_noteId;
    private boolean isSelected;
    private int m_size;
    private ViewGroup father = null;
    private int posX;
    private int posY;
    private boolean onEdit;
    private NoteResizer editLeft;
    private NoteResizer editRight;
    private static final int MIN_WIDTH = 0;
    private int clickX;
    private int clickY;
    private boolean onShadow;

    //private int height;
    public NoteView(Context context, PartitionLineView parent) {
        super(context);
        m_size=BASIC_SIZE;
        father = parent;
        editLeft = new NoteResizer(context, NoteResizer.LR.LEFT, this);
        editRight = new NoteResizer(context, NoteResizer.LR.RIGHT, this);
        editLeft.setVisibility(View.INVISIBLE);
        editRight.setVisibility(View.INVISIBLE);

        editLeft.setOnTouchListener(PartitionPageEventListener.getInstance().getTouchResizeListener());
        editRight.setOnTouchListener(PartitionPageEventListener.getInstance().getTouchResizeListener());
        addView(editLeft);
        addView(editRight);
    }




    public void setFather(ViewGroup view) { father = view; }


    public ViewGroup getFather() { return father; }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if(onEdit) {
            Paint select = new Paint(), border = new Paint();
            select.setColor(super.getContext().getResources().getColor(R.color.broken_white));
            border.setColor(super.getContext().getResources().getColor(R.color.green));
            canvas.drawRect(NoteResizer.WIDTH / 2 - 2, 0, getWidth() - NoteResizer.WIDTH / 2 + 2, canvas.getHeight() - 2, border);
            canvas.drawRect(NoteResizer.WIDTH / 2, 2, getWidth() - NoteResizer.WIDTH / 2, canvas.getHeight() - 4, select);
        } else {
            Paint pale = new Paint();
            pale.setColor(super.getContext().getResources().getColor(R.color.pale_blue));
            canvas.drawRect(NoteResizer.WIDTH / 2, 2, getWidth() - NoteResizer.WIDTH / 2, canvas.getHeight() - 4, pale);
        }
        super.dispatchDraw(canvas);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        //System.out.println(m_size + ":" + parentHeight);
        super.setMeasuredDimension(m_size, parentHeight-SPACE);
    }

    public int getSize() {
        return m_size;
    }

    public void setSize(int size) {
        this.m_size = size;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int x) {
        this.posX = x;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int y) {
        this.posY = y;
    }

    public void setOnEdit(boolean onEdit) {
        this.onEdit = onEdit;
        this.onShadow = false;
        if(onEdit) {
            editLeft.setVisibility(View.VISIBLE);
            editRight.setVisibility(View.VISIBLE);
        } else {
            editLeft.setVisibility(View.INVISIBLE);
            editRight.setVisibility(View.INVISIBLE);
        }
        invalidate();
    }

    public void resize(int size, NoteResizer.LR lr) {
        switch(lr){
            case LEFT:
                if(getRealSize() - size > MIN_WIDTH) {
                    if(size < 0) {
                        int pos = ((PartitionLineView)father).changePositionAndSizeRequest(this, posX + size);
                        int ecart = posX - pos;
                        posX = pos;
                        m_size += ecart;
                    } else {
                        posX += size;
                        m_size -= size;
                    }
                }
                break;
            case RIGHT:
                if(size > 0) {
                    if(getRealSize() + size > MIN_WIDTH) {
                        size = ((PartitionLineView)father).changeSizeRequest(this, getSize() + size);
                        m_size = size;
                    }
                } else {
                    if(getRealSize() + size > MIN_WIDTH) {
                        m_size += size;
                    }
                }
                break;
        }
        requestLayout();
    }

    public void setPosXWithCollisionVerif(int posX) {
        System.out.println("POS :: " + posX);
        if(((PartitionLineView)father).changePositionAndSizeRequest(this, posX, getRealSize())) {
            this.posX = posX;
        }
    }

    public void setSizeWithCollisionVerif(int size) {
        System.out.println("SIZE :: " + size);
        if(((PartitionLineView)father).changePositionAndSizeRequest(this, getRealPosX(), size)) {
            this.setSize(size + NoteResizer.WIDTH);
        }
    }


    public int getRealSize() {
        return m_size - NoteResizer.WIDTH;
    }

    public int getRealPosX() {
        return posX + NoteResizer.WIDTH / 2;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        editRight.layout(m_size - NoteResizer.WIDTH, getHeight() / 2 - (NoteResizer.HEIGHT / 2), m_size, getHeight() / 2 + (NoteResizer.HEIGHT / 2));
        editLeft.layout(0, getHeight() / 2 - (NoteResizer.HEIGHT / 2), NoteResizer.WIDTH, getHeight() / 2 + (NoteResizer.HEIGHT / 2));
    }

    public int getClickX() {
        return clickX;
    }

    public void setClickX(int clickX) {
        this.clickX = clickX;
    }

    public void setClickY(int clickY) {
        this.clickY = clickY;
    }

    public int getClickY() {
        return clickY;
    }

    public void setOnShadow(boolean onShadow) {
        this.onShadow = onShadow;
        this.onEdit = false;
    }
}