package univ.etu.soundroid.View.Partition.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.view.NoteView;

/**
 * Created by Jimmy on 24/11/2015.
 */
public class NoteResizer extends View {

    public static final int WIDTH = 20;
    public static final int HEIGHT = 50;

    private NoteView m_father;

    private int oldX;

    public int getOldX() {
        return oldX;
    }

    public void setOldX(int oldX) {
        this.oldX = oldX;
    }

    public NoteView getFather() {
        return m_father;
    }


    public enum LR { LEFT, RIGHT };
    private LR lr;

    public NoteResizer(Context context, LR lr, NoteView father) {
        super(context);
        this.lr = lr;
        this.m_father = father;
    }

    @Override
    protected void onDraw(Canvas canvas){
        Paint back = new Paint();
        back.setColor(super.getContext().getResources().getColor(R.color.green));
        canvas.drawCircle(WIDTH / 2, HEIGHT / 2 ,WIDTH/ 2,back);
    }

    public void resizeParent(int size){
        m_father.resize(size, this.lr);
    }


    public LR getLr() {
        return lr;
    }

    public void setLr(LR lr) {
        this.lr = lr;
    }

    @Override
    protected void onMeasure(int measureSpecWidth, int measureSpecHeight) {
        super.onMeasure(WIDTH, HEIGHT);
    }

}
