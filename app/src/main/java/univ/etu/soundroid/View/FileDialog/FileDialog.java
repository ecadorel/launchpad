package univ.etu.soundroid.View.FileDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import univ.etu.soundroid.Models.FilePath;
import univ.etu.soundroid.Models.Sound;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Welcome.event.WelcomeEventListener;

/**
 * Created by emile on 26/12/2015.
 */
public class FileDialog extends DialogFragment {

    public List<String> files = new ArrayList<String>();
    public String [] extension = null;
    public FilePath path = new FilePath();
    private String stringPath;
    private AdapterView.OnItemClickListener onItemClick;

    public void setFilePath(String path, String[] ext) {
        extension = ext;
        files = this.path.getFileName(path, ext);
        stringPath = path;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View container = inflater.inflate(R.layout.activity_welcome_load, null);
        ListView list = (ListView)container.findViewById(R.id.ChargerListView);

        TextView header = (TextView)container.findViewById(R.id.ChargerListHeader);
        TextView footer = (TextView)container.findViewById(R.id.ChargerListFooter);
        String headerText = "Choisir un fichier (";
        for(int i = 0; i < extension.length; i++) {
            headerText +=  extension[i];
            if(i < extension.length - 1) headerText += ", ";
        }

        header.setText(headerText + ")");
        footer.setText("  " + stringPath);
        fillList(list);


        builder.setView(container)
                .setNegativeButton(R.string.annuler, WelcomeEventListener.getInstance().getOnChargerAnnulation());

        return builder.create();
    }

    public void rewind() {
        if (!"/".equals(stringPath)) {
            String [] s = stringPath.split("/");
            stringPath = "";
            for(int i = 0; i < s.length - 1; i++) {
                stringPath += s[i];
                if(i < s.length - 2) stringPath += "/";
            }
            if("".equals(stringPath)) stringPath = "/";
        }
        files = this.path.getFileName(stringPath, extension);

        ListView list = (ListView)getDialog().findViewById(R.id.ChargerListView);
        TextView footer = (TextView)getDialog().findViewById(R.id.ChargerListFooter);
        footer.setText("  " + stringPath);
        fillList(list);

    }

    public void open(String filePath) {
        stringPath = filePath;
        files = this.path.getFileName(filePath, extension);

        ListView list = (ListView)getDialog().findViewById(R.id.ChargerListView);
        TextView footer = (TextView)getDialog().findViewById(R.id.ChargerListFooter);
        footer.setText("  " + stringPath);
        fillList(list);

    }

    private void fillList(ListView list) {
        List<Map<String,String>> soundListMap = new ArrayList<>();

        HashMap<String, String> elem;
        List<Integer> listArrow = new ArrayList<>();

        for(int i = 0; i < files.size(); i++) {
            elem = new HashMap<>();
            elem.put("FilePath", files.get(i));

            File f = new File(files.get(i));


            String [] dpaths = files.get(i).split("/");
            String filename;
            if(dpaths[dpaths.length - 1] == "/") filename = dpaths[dpaths.length - 2];
            else filename = dpaths[dpaths.length - 1];

            if("..".equals(filename)) listArrow.add(2);
            else if(f.isDirectory()) listArrow.add(0);
            else listArrow.add(1);

            elem.put("FileName", filename);
            soundListMap.add(elem);
        }

        ListAdapter adapter = new ImageAdapter(getActivity(), R.layout.file_dialog, R.id.fileDialog_text, R.id.fileDialog_arrow, soundListMap, "FileName", listArrow);
        list.setSelector(R.drawable.listselector);
        list.setAdapter(adapter);

        list.setOnItemClickListener(this.onItemClick);
    }


    public void setOnItemClick(AdapterView.OnItemClickListener onItemClick) {
        this.onItemClick = onItemClick;
    }
}
