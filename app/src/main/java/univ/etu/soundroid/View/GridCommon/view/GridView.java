package univ.etu.soundroid.View.GridCommon.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emile on 28/11/2015.
 */
public class GridView extends ViewGroup {

    private List<NoteView> m_listView = new ArrayList<>();


    private int screenWidth;
    private int screenHeight;

    public GridView(Context context) {
        super(context);
        Activity a = (Activity)this.getContext();
        Display display = a.getWindowManager().getDefaultDisplay();
        Point mSize = new Point();
        display.getSize(mSize);
        screenWidth = mSize.x;
        screenHeight = mSize.y;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();

        //je test pour chaque nombre de ligne possible, quel nombre de ligne maximise la taille des boutons en gardant la meilleure config
        //si je ne peux faire de carré (genre grille de 7 boutons), je centre le/les boutons de la dernière ligne
        int cols = 0;
        int rows = 0;
        int button_size = 0;
        int horizontal_shift = 0;
        int vertical_shift = 0;
        for(int test_rows = 1; test_rows <= count; test_rows++) {
            int test_cols = count/test_rows;
            if(count % test_rows != 0)
                test_cols++;
            int s1 = screenHeight/test_rows;
            int s2 = screenWidth/test_cols;
            int tmp = s1 < s2 ? s1 : s2;
            if(tmp > button_size) {
                button_size = tmp;
                cols = test_cols;
                rows = test_rows;
                horizontal_shift = screenWidth - (cols*button_size);
                vertical_shift = screenHeight - (rows*button_size);
            }
        }

        //partie affichage
        int x, y;
        int numCol = 0;
        int numRow = 0;
        int last_shift = 0; //si la dernière ligne comporte moins de touches que les précédentes, on va les centrer
        for(int i = 0; i < count; i++) {
            final NoteView child = (NoteView)getChildAt(i);
            if(child.getVisibility() != View.GONE) {
                //on est sur la dernière ligne, on vérifie si on doit faire une décallage particulier
                if(numRow == rows-1) {
                    if(count % rows != 0 && last_shift == 0) {
                        last_shift = (screenWidth -((count - (i+1))*button_size));
                        horizontal_shift = last_shift/2;
                    }
                }
                x = (horizontal_shift/2) + (numCol*button_size);
                y = (vertical_shift/2) + numRow*button_size;
                child.layout(x, y, x + button_size, y + button_size);
                child.setHeight(button_size);
                child.setWidth(button_size);
                numCol++;
                if(numCol == cols) {
                    numRow++;
                    numCol = 0;
                }
            }
        }
        invalidate();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }


}
