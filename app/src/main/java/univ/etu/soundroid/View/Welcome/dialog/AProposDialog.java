package univ.etu.soundroid.View.Welcome.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Welcome.event.WelcomeEventListener;

/**
 * Created by emile on 24/12/2015.
 */
public class AProposDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.activity_apropos_popup, null))
                .setNegativeButton(R.string.ok, WelcomeEventListener.getInstance().getOnProposValidation())
                .setPositiveButton(R.string.ecrire, WelcomeEventListener.getInstance().getOnProposEcrire());



        return builder.create();
    }

}
