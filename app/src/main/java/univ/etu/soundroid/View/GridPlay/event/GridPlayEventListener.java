package univ.etu.soundroid.View.GridPlay.event;

import android.view.MotionEvent;
import android.view.View;

import univ.etu.soundroid.Controller.GridPlayController;
import univ.etu.soundroid.View.GridCommon.view.NoteView;


/**
 * Created by emile on 29/11/2015.
 */
public class GridPlayEventListener {

    private static volatile GridPlayEventListener instance = null;
    private View.OnTouchListener onNoteTouchListener;

    public static GridPlayEventListener getInstance() {
        if(instance == null) {
            instance = new GridPlayEventListener();
        }
        return instance;
    }


    private GridPlayEventListener() {
        onNoteTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                NoteView button = (NoteView)v;
                int action = event.getActionMasked();

                if(action == MotionEvent.ACTION_UP) {
                    GridPlayController.getInstance().getConductor().addSoundToStop(button.getId());
                } else {
                    GridPlayController.getInstance().getConductor().addSoundToPlay(button.getId());
                }

                return true;
            }
        };
    }


    public View.OnTouchListener getOnNoteTouchListener() {
        return onNoteTouchListener;
    }
}
