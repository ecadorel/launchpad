package univ.etu.soundroid.View.Welcome.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Welcome.event.WelcomeEventListener;


public class WelcomeDialog extends DialogFragment {

   @Override
   public Dialog onCreateDialog(Bundle savedInstanceState) {
       AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       LayoutInflater inflater = getActivity().getLayoutInflater();

       builder.setView(inflater.inflate(R.layout.activity_welcome_popup, null))
               .setPositiveButton(R.string.creer, WelcomeEventListener.getInstance().getOnPopupValidation())
               .setNegativeButton(R.string.annuler, WelcomeEventListener.getInstance().getOnPopupAnnulation());



       return builder.create();
   }

}
