package univ.etu.soundroid.View.Partition.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.ViewGroup;

import univ.etu.soundroid.R;

/**
 * Created by Jimmy on 23/11/2015.
 */
public class PartitionBin extends ViewGroup {

    private static int size = 100;
    private static int bias = 10;
    private static int littleBias = 5;

    public PartitionBin(Context context) {
        super(context);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        this.removeAllViews();
    }

    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        super.setMeasuredDimension(size, size);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        Paint back = new Paint(), in = new Paint();
        back.setColor(super.getContext().getResources().getColor(R.color.red));
        in.setColor(super.getContext().getResources().getColor(R.color.white));
        canvas.drawRect(0, 0, size, size, back);
        canvas.drawRect(littleBias, littleBias, size - littleBias, size - littleBias, in);

        back.setStrokeWidth(5);
        canvas.drawLine(bias * 2, bias * 2, size - bias * 2, size - bias * 2, back);
        canvas.drawLine(size - bias * 2, bias * 2, bias * 2, size - bias * 2, back);


        super.dispatchDraw(canvas);
    }
}
