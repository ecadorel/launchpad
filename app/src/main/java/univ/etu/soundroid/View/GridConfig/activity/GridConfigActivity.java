package univ.etu.soundroid.View.GridConfig.activity;


import android.app.Activity;
import android.os.Bundle;

import univ.etu.soundroid.Controller.GridConfigController;
import univ.etu.soundroid.R;

public class GridConfigActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_config);
        GridConfigController.getInstance().set(this);
    }

}
