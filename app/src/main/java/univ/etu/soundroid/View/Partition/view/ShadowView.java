package univ.etu.soundroid.View.Partition.view;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;

import univ.etu.soundroid.R;

/**
 * Created by emile on 28/12/2015.
 */
public class ShadowView extends ViewGroup {

    private static final int DIAMETER = 100;
    private static final int RADIUS = 50;

    public ShadowView(Activity activity) {
        super(activity);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {

        Paint p = new Paint();
        p.setColor(super.getContext().getResources().getColor(R.color.blue));
        canvas.drawCircle(RADIUS, RADIUS, DIAMETER, p);
        super.dispatchDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        System.out.println("MESURE DIMENSION");
        super.setMeasuredDimension(DIAMETER, DIAMETER);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }
}
