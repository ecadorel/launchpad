package univ.etu.soundroid.View.Partition.activity;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import univ.etu.soundroid.Controller.NoteEditController;
import univ.etu.soundroid.Controller.PartitionController;
import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.Models.NoteGrid;
import univ.etu.soundroid.R;

/**
 * Created by emile on 28/11/2015.
 */
public class PartitionActivity extends TabActivity {


    private TabHost tabHost;
    List<Integer> openTab = new ArrayList<>();
    public Map<Integer, NoteEditActivity> openActivity = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partition_view);

        this.tabHost = getTabHost();
        PartitionController.getInstance().set(this);

        //On est sur la premiere page on init les donnees
        if(GlobalData.getInstance().getGrid() == null) {
            GlobalData.getInstance().setGrid(new NoteGrid());
        }
    }

    public TabHost getLayout() {
        return tabHost;
    }

    private void setupTab(String name, String tag, Intent intent) {
        TabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setContent(intent);
        tabSpec.setIndicator(name);
        tabHost.addTab(tabSpec);
    }

    public void addTab(int id) {
        if(openTab.lastIndexOf(id) == - 1) {
            Intent intent = new Intent(this, NoteEditActivity.class);
            intent.putExtra("NoteId", id);
            setupTab(Integer.toString(id), Integer.toString(id), intent);
            openTab.add(id);
        }
        tabHost.setCurrentTab(openTab.indexOf(id));
    }


    public void removeTab(int note) {
        tabHost.clearAllTabs();

        int tab = openTab.lastIndexOf(note);
        if(tab != -1) {
            openTab.remove(tab);
        }

        for(int i = 0; i < openTab.size(); i++) {
            brutAddTab(openTab.get(i));
        }
    }

    private void brutAddTab(int id) {
        Intent intent = new Intent(this, NoteEditActivity.class);
        intent.putExtra("NoteId", id);
        setupTab(Integer.toString(id), Integer.toString(id), intent);
        tabHost.setCurrentTab(openTab.indexOf(id));
    }

    public boolean isOpen(int noteId) {
        return openTab.lastIndexOf(noteId) != -1;
    }
}

