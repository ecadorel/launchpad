package univ.etu.soundroid.View.Partition.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Controller.SoundChoiceController;
import univ.etu.soundroid.R;

/**
 * Created by Guillaume on 20/12/2015.
 */
public class SoundChoiceActivity extends Activity {
    private static final int FILE_SELECT_CODE = 0;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.sound_choice);
        SoundChoiceController.getInstance().set(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String path = uri.getPath();
                    try {
                        SoundChoiceController.getInstance().addSound(path);
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
