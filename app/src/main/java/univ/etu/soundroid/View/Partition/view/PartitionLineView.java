package univ.etu.soundroid.View.Partition.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

import univ.etu.soundroid.R;


/**
 * Created by emile on 29/11/2015.
 */
public class PartitionLineView extends ViewGroup {


    private int noteId;
    private int m_height = 100;
    private int m_width = 2030;
    private int BASIC_BIAS = 3;
    private int scrollBegin = 0;
    public static int BEGIN_LINE = 30;
    private float clickPosX;
    private boolean onShadow = false;

    public PartitionLineView(Context context, int noteId) {
        super(context);
        this.noteId = noteId;
    }

    public PartitionLineView(Context context, int noteId, int width) {
        super(context);
        this.noteId = noteId;
        this.m_width = width + BEGIN_LINE + NoteResizer.WIDTH / 2;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();
        for(int i = 0; i < count; i++) {
            final NoteView child = (NoteView)getChildAt(i);
            if (child.getVisibility() != View.GONE) {
                child.layout(child.getPosX() - scrollBegin, 0, child.getSize()+child.getPosX() - scrollBegin, getHeight()-4);
            }
        }
        invalidate();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        Paint back = new Paint(), black = new Paint();
        back.setColor(super.getContext().getResources().getColor(R.color.materialcolorpicker__lightgrey));
        black.setColor(super.getContext().getResources().getColor(R.color.black));
        canvas.drawRect(BEGIN_LINE - scrollBegin, 0, width - NoteResizer.WIDTH / 2, height - 5, back);
        super.dispatchDraw(canvas);
        back.setColor(super.getContext().getResources().getColor(R.color.purple));
        canvas.drawRect(BEGIN_LINE - 3, 0, BEGIN_LINE, height - 5, back);
        Rect bounds = new Rect();
        black.getTextBounds(Integer.toString(noteId), 0, Integer.toString(noteId).length(), bounds);
        canvas.drawText(Integer.toString(noteId), BEGIN_LINE / 2 - bounds.width() / 2, height / 2, black);
        if(onShadow) {
            back.setColor(super.getContext().getResources().getColor(R.color.red));
            back.setAlpha(100);
            canvas.drawRect(0, 0, width, height - 5, back);
        }
    }

    /**
     * Demande depuis une note view le changement de sa taille
     * A appeler que si le changement est positif
     * @param who la note view qui va grandir
     * @param size la nouvelle taille
     * @return l'autorisation de changement de taille ou non
     * */
    public int changeSizeRequest(NoteView who, int size) {
        if(who.getRealPosX() + size > m_width) {
            return m_width - who.getPosX();
        }
        for (int i = 0; i < getChildCount(); i++) {
            NoteView aux = (NoteView) getChildAt(i);
            if (aux != who) {//test l'intersection
                if (who.getRealPosX() < aux.getRealPosX()) { //On a un element a droite
                    if (aux.getRealPosX() < who.getRealPosX() + size + BASIC_BIAS)
                        return aux.getRealPosX() - BASIC_BIAS - who.getRealPosX() + NoteResizer.WIDTH;
                }
            }
        }
        return size;
    }

    public int changePositionAndSizeRequest(NoteView who, int pos) {
        if(pos + (NoteResizer.WIDTH / 2) >= BEGIN_LINE) {
            for (int i = 0; i < getChildCount(); i++) {
                NoteView aux = (NoteView) getChildAt(i);
                if (aux != who) {
                    if (pos >= aux.getRealPosX()) { //on a un element a gauche
                        if (aux.getRealPosX() + aux.getRealSize() > pos + BASIC_BIAS) {
                            return  aux.getPosX() + aux.getRealSize() + BASIC_BIAS;
                        }
                    }
                }
            }
            return pos;
        }
        return BEGIN_LINE - NoteResizer.WIDTH / 2;
    }

    public boolean changePositionAndSizeRequest(NoteView who, int pos, int size) {
        for(int i = 0; i < getChildCount(); i++) {
            NoteView aux = (NoteView)getChildAt(i);
            if(aux != who) {
                if(pos >= aux.getRealPosX()) { //on a un element a gauche
                    if(aux.getRealPosX() + aux.getRealSize() > pos + BASIC_BIAS) {
                        return false;
                    }
                } else {
                    if(aux.getRealPosX() > pos + size + BASIC_BIAS) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    /**
     * Retourne si la nouvelle note va etre en intersection avec les autres
     * */
    public boolean intersect(NoteView note, int position, int size) {
        position += scrollBegin;
        if(position < BEGIN_LINE || position + size > m_width - NoteResizer.WIDTH / 2) return false;
        for(int i = 0; i < getChildCount(); i++) {
            NoteView aux = (NoteView)getChildAt(i);
            if(note != aux) {
                if (position > aux.getRealPosX() - BASIC_BIAS) {
                    if (aux.getRealSize() + aux.getRealPosX() + BASIC_BIAS > position) return false;
                } else if (position < aux.getRealPosX() + BASIC_BIAS) {
                    if (aux.getRealPosX() < size + position + BASIC_BIAS) return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        if(!onShadow) {
            super.setMeasuredDimension(m_width - scrollBegin, m_height);
            final int count = getChildCount();
            for (int i = 0; i < count; i++) {
                final View child = getChildAt(i);
                if (child.getVisibility() != View.GONE) {
                    measureChild(child, widthMeasureSpec, heightMeasureSpec);
                }
            }
        } else {
            super.setMeasuredDimension(100, 100);
        }
    }

    public boolean addNoteView(NoteView v) {
        if(!intersect(v, v.getRealPosX(), v.getRealSize()) || v.getPosX() < BEGIN_LINE) {
            return false;
        } else {
            v.setPosX(v.getPosX() + scrollBegin);
            addView(v);
            return true;
        }
    }

    public int getNoteId() {
        return noteId;
    }

    public int getLen() {
        return m_width;
    }

    public void setScrollBegin(int scrollBegin) {
        this.scrollBegin = scrollBegin;
    }

    public void setClickPosX(float clickPosX) {
        this.clickPosX = clickPosX;
    }

    public float getClickPosX() {
        return clickPosX;
    }

    public int getScrollBegin() {
        return scrollBegin;
    }

    public void setOnShadow(boolean onShadow) {
        this.onShadow = onShadow;
        invalidate();
    }

    public boolean isOnShadow() {
        return onShadow;
    }
}
