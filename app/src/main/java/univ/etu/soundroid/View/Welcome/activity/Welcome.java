package univ.etu.soundroid.View.Welcome.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.activity.PartitionActivity;

/**
 * Created by root on 25/11/2015.
 */
public class Welcome extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        //Ceci est un commentaire

        final Button buttnouveau = (Button) findViewById(R.id.buttNouveau);
        buttnouveau.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            Intent intent = new Intent(Welcome.this, PartitionActivity.class);
                startActivity(intent);
            }
        });
    }
}