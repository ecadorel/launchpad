package univ.etu.soundroid.View.Welcome.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import univ.etu.soundroid.Controller.WelcomeController;
import univ.etu.soundroid.Models.FileTool;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.Partition.activity.PartitionActivity;

/**
 * Created by root on 25/11/2015.
 */
public class WelcomeActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        //Ceci est un commentaire


    }

    @Override
    public void onResume() {
        super.onResume();
        WelcomeController.getInstance().set(this);
    }

}