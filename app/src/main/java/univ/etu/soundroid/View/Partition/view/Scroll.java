package univ.etu.soundroid.View.Partition.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import univ.etu.soundroid.Controller.PartitionPageController;
import univ.etu.soundroid.R;

/**
 * Created by emile on 22/12/2015.
 */
public class Scroll extends View {

    private int pos = 0;
    private int size;
    private int oldX;

    public Scroll(Context context, int size) {
        super(context);
        this.size = size;
    }


    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public void onDraw(Canvas canvas) {
        Paint back = new Paint();
        back.setColor(super.getContext().getResources().getColor(R.color.pale_green));
        canvas.drawRect(0, 0, size, getHeight(), back);
        super.onDraw(canvas);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heigthMeasureSpec) {
        int height = MeasureSpec.getSize(heigthMeasureSpec);
        super.onMeasure(size, height);
    }


    public void setOldX(int oldX) {
        this.oldX = oldX;
    }

    public int getOldX() {
        return oldX;
    }

    public void move(int position) {
        if(((PartitionBottomScroll)getParent()).requestMove(pos + position, size)) {
            pos = pos + position;
            requestLayout();
            PartitionPageController.getInstance().scrollLine(pos * ((PartitionBottomScroll) getParent()).getScrollSize() / ((PartitionBottomScroll) getParent()).getWidth());
        }
    }

    public void resetPosition() {
        pos = 0;
        requestLayout();
        PartitionPageController.getInstance().scrollLine(0);
    }
}
