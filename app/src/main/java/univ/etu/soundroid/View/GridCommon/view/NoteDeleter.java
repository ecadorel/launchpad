package univ.etu.soundroid.View.GridCommon.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

import univ.etu.soundroid.R;

/**
 * Created by emile on 31/12/2015.
 */
public class NoteDeleter extends View {

    private final static int WIDTH = 40,
                             HEIGHT = 40;


    public NoteDeleter(Context context) {
        super(context);
    }

    @Override
    public void onDraw(Canvas canvas) {

        Paint p = new Paint();
        p.setColor(getContext().getResources().getColor(R.color.red));
        p.setAntiAlias(true);
        p.setStyle(Paint.Style.FILL);

        canvas.drawRect(0, 0, WIDTH, HEIGHT, p);
        p.setStrokeWidth(5);
        p.setColor(getContext().getResources().getColor(R.color.materialcolorpicker__lightgrey));
        canvas.drawLine(5, 5, 35, 35, p);
        canvas.drawLine(35, 5, 5, 35, p);
        super.onDraw(canvas);
    }


    @Override
    public void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(WIDTH, HEIGHT);
    }


}
