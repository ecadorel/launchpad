package univ.etu.soundroid.View.Partition.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Controller.NoteEditController;
import univ.etu.soundroid.Controller.PartitionController;
import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.R;

/**
 * Created by emile on 28/11/2015.
 */
public class NoteEditActivity extends Activity {

    private int noteId = 0;

    private LinearLayout mainContainer = null;
    private boolean isOnInfo = true;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.tab_item);
        mainContainer = (LinearLayout)findViewById(R.id.NoteEditMainContainer);
        noteId = getIntent().getExtras().getInt("NoteId");
        NoteEditController.getInstance().setId(noteId);
        ((PartitionActivity)PartitionController.getInstance().getActivity()).openActivity.put(noteId, this);
    }

    /**
     * lorsque qu'on appuie sur l'onglet
     */
    @Override
    protected void onResume() {
        super.onResume();
        NoteEditController.getInstance().set(this);
        NoteEditController.getInstance().setId(noteId);
        if(isOnInfo) {
            try {
                setOnInformation();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        } else {
            setOnPartition();
        }
    }


    public void setOnPartition() {

        isOnInfo = false;
        mainContainer.removeAllViews();
        View v = getLayoutInflater().inflate(R.layout.partition_page, null);
        mainContainer.addView(v);
        NoteEditController.getInstance().onPartition();

    }

    public void setOnInformation() throws ParserConfigurationException {
        isOnInfo = true;
        mainContainer.removeAllViews();
        View v = getLayoutInflater().inflate(R.layout.note_edit, null);
        mainContainer.addView(getLayoutInflater().inflate(R.layout.note_edit, null));
        NoteEditController.getInstance().onInformation();

    }

    void setNoteId(int id) {
        noteId = id;
    }

    public int getNoteId() {
        return noteId;
    }
}
