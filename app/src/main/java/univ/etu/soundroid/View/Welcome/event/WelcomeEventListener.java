package univ.etu.soundroid.View.Welcome.event;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.internal.view.menu.MenuView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;

import univ.etu.soundroid.Controller.WelcomeController;
import univ.etu.soundroid.Models.FilePath;
import univ.etu.soundroid.Models.GlobalData;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.FileDialog.FileDialog;
import univ.etu.soundroid.View.Partition.activity.PartitionActivity;
import univ.etu.soundroid.View.Welcome.activity.Welcome;
import univ.etu.soundroid.View.Welcome.dialog.AProposDialog;
import univ.etu.soundroid.View.Welcome.dialog.WelcomeDialog;

/**
 * Created by emile on 24/12/2015.
 */
public class WelcomeEventListener {

    private static volatile WelcomeEventListener instance = null;
    private View.OnClickListener onNouveau;
    private DialogInterface.OnClickListener onPopupValidation;
    private DialogInterface.OnClickListener onPopupAnnulation;
    private DialogInterface.OnClickListener onProposValidation;
    private View.OnClickListener onPropos;
    private AdapterView.OnItemClickListener onItemClickListener;
    private View.OnClickListener onCharger;
    private DialogInterface.OnClickListener onChargerValidation;
    private DialogInterface.OnClickListener onChargerAnnulation;
    private AdapterView.OnItemClickListener chargerOnItemClick;
    private DialogInterface.OnClickListener onProposEcrire;

    public static WelcomeEventListener getInstance() {
        if(instance == null) {
            instance = new WelcomeEventListener();
        }
        return instance;
    }

    private WelcomeEventListener() {

        onNouveau = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WelcomeController.getInstance().setDialog(new WelcomeDialog());
                WelcomeController.getInstance().getDialog().show(WelcomeController.getInstance().getActivity().getFragmentManager(), "dialog");
            }
        };

        onPopupAnnulation = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        };

        onPopupValidation = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                WelcomeDialog wel = WelcomeController.getInstance().getDialog();
                String name = ((TextView)wel.getDialog().findViewById(R.id.WelcomePopupName)).getText().toString();
                if(!"".equals(name)) {
                    WelcomeController.getInstance().openProject(name, true);
                }
            }
        };


        onPropos = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WelcomeController.getInstance().setAppdialog(new AProposDialog());
                WelcomeController.getInstance().getAppdialog().show(WelcomeController.getInstance().getActivity().getFragmentManager(), "dialog");
            }
        };

        onProposValidation = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        };

        onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String,String> elem = (HashMap<String,String>)parent.getAdapter().getItem(position);
                String path = elem.get("ProjectFile");
                WelcomeController.getInstance().openProjectWithPath(path);
            }
        };

        onCharger = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WelcomeController.getInstance().setChargerDialog(new FileDialog());
                WelcomeController.getInstance().getChargerDialog().setOnItemClick(WelcomeEventListener.this.chargerOnItemClick);
                WelcomeController.getInstance().getChargerDialog().setFilePath(WelcomeController.BASIC_PATH, new String[]{".xml"});
                WelcomeController.getInstance().getChargerDialog().show(WelcomeController.getInstance().getActivity().getFragmentManager(), "dialog");
            }
        };

        chargerOnItemClick = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> elem = (HashMap<String, String>)parent.getItemAtPosition(position);
                view.setBackgroundColor(WelcomeController.getInstance().getActivity().getResources().getColor(R.color.pale_green));
                if(elem.get("FileName").equals("..")) {
                    WelcomeController.getInstance().getChargerDialog().rewind();
                } else {
                    File f = new File(elem.get("FilePath"));
                    if(f.isDirectory()) {
                        WelcomeController.getInstance().getChargerDialog().open(elem.get("FilePath"));
                    } else {
                        WelcomeController.getInstance().getChargerDialog().getDialog().cancel();
                        WelcomeController.getInstance().openProjectWithPath(elem.get("FilePath"));
                    }
                }
            }

        };

        onProposEcrire = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"valentin.bouziat@etu.univ-orleans.fr",
                                                            "emile.cadorel@etu.univ-orleans.fr",
                                                            "jimmy.furet@etu.univ-orleans.fr",
                                                            "guillaume.gas@etu.univ-orleans.fr" });
                try {
                    WelcomeController.getInstance().getActivity().startActivity(Intent.createChooser(i, "Envoyer un mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(WelcomeController.getInstance().getActivity(), "Erreur.", Toast.LENGTH_LONG);
                }
            }
        };

    }

    public View.OnClickListener getOnNouveau() {
        return onNouveau;
    }

    public DialogInterface.OnClickListener getOnPopupValidation() {
        return onPopupValidation;
    }

    public DialogInterface.OnClickListener getOnPopupAnnulation() {
        return onPopupAnnulation;
    }

    public DialogInterface.OnClickListener getOnProposValidation() {
        return onProposValidation;
    }

    public View.OnClickListener getOnPropos() {
        return onPropos;
    }

    public AdapterView.OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public View.OnClickListener getOnCharger() {
        return onCharger;
    }

    public DialogInterface.OnClickListener getOnChargerValidation() {
        return onChargerValidation;
    }

    public DialogInterface.OnClickListener getOnChargerAnnulation() {
        return onChargerAnnulation;
    }

    public AdapterView.OnItemClickListener getChargerOnItemClick() {
        return chargerOnItemClick;
    }

    public DialogInterface.OnClickListener getOnProposEcrire() {
        return onProposEcrire;
    }
}
