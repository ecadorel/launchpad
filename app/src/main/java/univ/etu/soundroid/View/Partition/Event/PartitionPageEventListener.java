package univ.etu.soundroid.View.Partition.Event;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Controller.GridConfigController;
import univ.etu.soundroid.Controller.NoteEditController;
import univ.etu.soundroid.Controller.PartitionPageController;
import univ.etu.soundroid.Models.Partition;
import univ.etu.soundroid.R;
import univ.etu.soundroid.View.GridConfig.activity.GridConfigActivity;
import univ.etu.soundroid.View.Partition.view.NoteResizer;
import univ.etu.soundroid.View.Partition.view.NoteView;
import univ.etu.soundroid.View.Partition.view.PartitionLineView;
import univ.etu.soundroid.View.Partition.view.Scroll;
import univ.etu.soundroid.View.Partition.view.ShadowView;

/**
 * Created by emile on 28/11/2015.
 */
public class PartitionPageEventListener {

    private static volatile PartitionPageEventListener instance = null;
    private View.OnTouchListener onAjouterLigneListener;
    private View.OnTouchListener touchResizeListener;
    private View.OnTouchListener onLineTouchListener;
    private View.OnTouchListener onTouchNoteListener;
    private View.OnLongClickListener longNoteClickListener;
    private View.OnDragListener onDragListener;
    private TextWatcher textPositionChangeListener;
    private TextWatcher textLongueurChangeListener;
    private TextWatcher textTotalChangeListener;
    private View.OnTouchListener ScrollTouchListener;
    private View.OnTouchListener onSupprLineListener;

    public static PartitionPageEventListener getInstance() {
        if(instance == null) {
            instance = new PartitionPageEventListener();
        }
        return instance;
    }

    private PartitionPageEventListener() {

        onAjouterLigneListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(PartitionPageController.getInstance().getActivity(), GridConfigActivity.class);
                    intent.putExtra("mode", GridConfigController.GridConfigState.LINE);
                    intent.putExtra("noteId", NoteEditController.getInstance().getNoteId());
                    PartitionPageController.getInstance().getActivity().startActivity(intent);
                }

                return false;
            }
        };

        touchResizeListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                NoteResizer view = (NoteResizer)v;
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setOldX((int)event.getRawX());
                }
                if(event.getAction() == MotionEvent.ACTION_MOVE){
                    int size, x = (int)event.getRawX();
                    size = x - view.getOldX();
                    view.resizeParent(size);
                    view.setOldX(x);
                }
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    int size, x = (int)event.getRawX();
                    size = x - view.getOldX();
                    view.resizeParent(size);
                    try {
                        PartitionPageController.getInstance().lineChange((PartitionLineView) view.getFather().getFather());
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                    view.setOldX(x);
                }
                return true;
            }
        };

        ScrollTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Scroll view = (Scroll)v;
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    view.setOldX((int)event.getRawX());
                }
                if(event.getAction() == MotionEvent.ACTION_MOVE){
                    int size, x = (int)event.getRawX();
                    size = x - view.getOldX();
                    view.move(size);
                    view.setOldX(x);
                }
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    int size, x = (int)event.getRawX();
                    size = x - view.getOldX();
                    view.move(size);
                    view.setOldX(x);
                }
                return true;
            }
        };


        onTouchNoteListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    PartitionPageController.getInstance().setOnEdit((NoteView) v, true);
                    ((NoteView)v).setClickX((int) event.getRawX());
                    ((NoteView)v).setClickY((int) event.getRawY());
                }
                return false;
            }
        };

        longNoteClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipData data = ClipData.newPlainText("", "");
                PartitionPageController.getInstance().setOnEdit((NoteView) v, false);
                final int x = ((NoteView)v).getClickX() ,
                          y = ((NoteView)v).getClickY();

                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);

                v.startDrag(data, shadowBuilder, v, 0);
                v.setVisibility(View.INVISIBLE);
                PartitionPageController.getInstance().setOnEdit((NoteView) v, false);
                return true;

            }
        };

        onLineTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    NoteView view = new NoteView(PartitionPageController.getInstance().getActivity(), (PartitionLineView) v);
                    view.setPosX((int) event.getRawX() - view.getRealSize() / 2);
                    if (!((PartitionLineView) v).isOnShadow()) {
                        if (((PartitionLineView) v).addNoteView(view)) {
                            view.setOnTouchListener(getOnTouchNoteListener());
                            view.setLongClickable(true);
                            view.setOnLongClickListener(getLongNoteClickListener());
                            try {
                                PartitionPageController.getInstance().lineChange((PartitionLineView) v);
                            } catch (ParserConfigurationException e) {
                                e.printStackTrace();
                            }
                        } else if (event.getRawX() < PartitionLineView.BEGIN_LINE) {
                            PartitionPageController.getInstance().setLineSelected(((PartitionLineView)v));
                        }
                    } else {
                        PartitionPageController.getInstance().deselectLine();
                    }
                }

                return false;
            }
        };

        onDragListener = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        PartitionPageController.getInstance().getPartitionBin().setVisibility(View.VISIBLE);
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        //v.setBackgroundDrawable(enterShape);
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        //v.setBackgroundDrawable(normalShape);
                        break;
                    case DragEvent.ACTION_DROP:
                        if(event.getLocalState() instanceof NoteView) {
                            NoteView view = (NoteView) event.getLocalState();
                            if (v != null && view != null) { //Verification de l'Enfer
                                PartitionPageController.getInstance().getPartitionBin().setVisibility(View.INVISIBLE);
                                if (v == PartitionPageController.getInstance().getPartitionBin() || v == view.getFather()) {
                                    if (v == view.getFather()) {
                                        if (!((PartitionLineView) v).intersect(view, (int) event.getX() - view.getSize() / 2, view.getRealSize())) {
                                            return false;
                                        }
                                    }
                                    if (v != PartitionPageController.getInstance().getPartitionBin())
                                        view.setPosX((int) event.getX() - view.getSize() / 2 + ((PartitionLineView) v).getScrollBegin());
                                    view.getFather().removeView(view);
                                    view.setFather((ViewGroup) v);
                                    ((ViewGroup) v).addView(view);
                                    view.setVisibility(View.VISIBLE);
                                    if (v instanceof PartitionLineView) {
                                        try {
                                            PartitionPageController.getInstance().lineChange((PartitionLineView) v);
                                        } catch (ParserConfigurationException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    return false;
                                }
                            }
                        }
                        return false;
                    case DragEvent.ACTION_DRAG_ENDED:
                        if(event.getLocalState() instanceof NoteView) {
                            NoteView _view_ = (NoteView) event.getLocalState();
                            _view_.setVisibility(View.VISIBLE);
                            PartitionPageController.getInstance().setOnEdit(_view_, true);
                        }
                        //v.setBackgroundDrawable(normalShape);
                    default:
                        break;
                }
                return true;

            }
        };

        textTotalChangeListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if(!"".equals(text)) {
                    try {
                        PartitionPageController.getInstance().changeLinesLength(Integer.parseInt(text));
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        PartitionPageController.getInstance().changeLinesLength(0);
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        onSupprLineListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        PartitionPageController.getInstance().removeLine();
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        };

    }



    public View.OnTouchListener getOnAjouterLigneListener() {
        return onAjouterLigneListener;
    }

    public View.OnTouchListener getTouchResizeListener() {
        return touchResizeListener;
    }

    public View.OnTouchListener getOnLineTouchListener() {
        return onLineTouchListener;
    }

    public View.OnTouchListener getOnTouchNoteListener() {
        return onTouchNoteListener;
    }

    public View.OnLongClickListener getLongNoteClickListener() {
        return longNoteClickListener;
    }

    public View.OnDragListener getOnDragListener() {
        return onDragListener;
    }

    public TextWatcher getTextPositionChangeListener() {
        return textPositionChangeListener;
    }

    public TextWatcher getTextLongueurChangeListener() {
        return textLongueurChangeListener;
    }

    public TextWatcher getTextTotalChangeListener() {
        return textTotalChangeListener;
    }

    public View.OnTouchListener onScrollTouchListener() {
        return ScrollTouchListener;
    }

    public View.OnTouchListener getOnSupprLineListener() {
        return onSupprLineListener;
    }
}
