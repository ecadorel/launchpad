package univ.etu.soundroid.View.Partition.Event;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Controller.SoundChoiceController;
import univ.etu.soundroid.Controller.WelcomeController;
import univ.etu.soundroid.Models.FileTool;
import univ.etu.soundroid.View.FileDialog.FileDialog;

/**
 * Created by Guillaume on 20/12/2015.
 */
public class SoundChoiceEventListener {
    private static volatile SoundChoiceEventListener instance = null;

    private AdapterView.OnItemClickListener onItemClickListener;
    private View.OnTouchListener onLoadFileListener;
    public AdapterView.OnItemClickListener onChargerItemClickListener;

    public static SoundChoiceEventListener getInstance() {
        if(instance == null) {
            instance = new SoundChoiceEventListener();
        }
        return instance;
    }

    public SoundChoiceEventListener() {
        onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int soundPosition = position;
                SoundChoiceController.getInstance().setSelectedSound(position);
            }
        };

        onLoadFileListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    SoundChoiceController.getInstance().setChargerDialog(new FileDialog());
                    SoundChoiceController.getInstance().getChargerDialog().setOnItemClick(SoundChoiceEventListener.this.onChargerItemClickListener);
                    SoundChoiceController.getInstance().getChargerDialog().setFilePath(SoundChoiceController.BASIC_PATH, new String[]{".mp3" , ".aac", ".wav"});
                    SoundChoiceController.getInstance().getChargerDialog().show(SoundChoiceController.getInstance().getActivity().getFragmentManager(), "dialog");
                }
                return true;
            }
        };


        onChargerItemClickListener = new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> elem = (HashMap<String, String>) parent.getItemAtPosition(position);
                if (elem.get("FileName").equals("..")) {
                    SoundChoiceController.getInstance().getChargerDialog().rewind();
                } else {
                    File f = new File(elem.get("FilePath"));
                    if (f.isDirectory()) {
                        SoundChoiceController.getInstance().getChargerDialog().open(elem.get("FilePath"));
                    } else {
                        SoundChoiceController.getInstance().getChargerDialog().getDialog().cancel();
                        try {
                            SoundChoiceController.getInstance().addSound(elem.get("FilePath"));
                        } catch (ParserConfigurationException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        };

    }

    public AdapterView.OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }
    public View.OnTouchListener getOnLoadFileListener() { return onLoadFileListener; }
}
