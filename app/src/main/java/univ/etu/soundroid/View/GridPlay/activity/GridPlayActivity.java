package univ.etu.soundroid.View.GridPlay.activity;

import android.app.Activity;
import android.os.Bundle;

import univ.etu.soundroid.Controller.GridPlayController;
import univ.etu.soundroid.Controller.SoundController;
import univ.etu.soundroid.R;

/**
 * Created by emile on 28/11/2015.
 */
public class GridPlayActivity extends Activity {


    @Override
    public void onCreate(Bundle info) {
        super.onCreate(info);
        setContentView(R.layout.grid_play);
        GridPlayController.getInstance().set(this);
        SoundController.getInstance().set(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        GridPlayController.getInstance().onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        GridPlayController.getInstance().onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        GridPlayController.getInstance().onFinish();
    }

}
