package univ.etu.soundroid.View.GridConfig.event;

import android.view.MotionEvent;
import android.view.View;

import javax.xml.parsers.ParserConfigurationException;

import univ.etu.soundroid.Controller.GridConfigController;
import univ.etu.soundroid.Controller.PartitionController;
import univ.etu.soundroid.Controller.PartitionPageController;
import univ.etu.soundroid.View.GridCommon.view.NoteView;

/**
 * Created by emile on 28/11/2015.
 */
public class GridConfigEventListener {

    private static volatile GridConfigEventListener instance = null;
    private View.OnTouchListener onNoteTouchListener;
    private View.OnTouchListener onAddTouchListener;
    private View.OnTouchListener onNoteForLineTouchListener;
    private View.OnTouchListener onDeleteNoteListener;

    public static GridConfigEventListener getInstance() {
        if(instance == null) {
            instance = new GridConfigEventListener();
        }
        return instance;
    }

    private GridConfigEventListener() {

        onAddTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( ((NoteView)v).isActive() ) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        ((NoteView) v).setIsTouch(true);
                        v.invalidate();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        ((NoteView) v).setIsTouch(false);
                        v.invalidate();

                        try {
                            int id = GridConfigController.getInstance().addNote();
                            GridConfigController.getInstance().getActivity().finish();
                            PartitionController.getInstance().addTab(id);
                        } catch (ParserConfigurationException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return true;
            }
        };

        onNoteTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( ((NoteView)v).isActive() ) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        ((NoteView) v).setIsTouch(true);
                        v.invalidate();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        ((NoteView) v).setIsTouch(false);
                        v.invalidate();
                        PartitionController.getInstance().addTab(((NoteView) v).getId());
                        GridConfigController.getInstance().getActivity().finish();
                    }
                }
                return true;
            }
        };


        onNoteForLineTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ((NoteView) v).setIsTouch(true);
                    v.invalidate();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ((NoteView) v).setIsTouch(false);
                    v.invalidate();
                    PartitionPageController.getInstance().addLine(((NoteView)v).getId());
                    GridConfigController.getInstance().getActivity().finish();
                }
                return true;
            }
        };

        onDeleteNoteListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    NoteView view = (NoteView) v.getParent();

                    try {
                        GridConfigController.getInstance().removeNote(view.getId());
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                }

                return true;
            }
        };


    }



    public View.OnTouchListener getOnNoteTouchListener() {
        return onNoteTouchListener;
    }

    public View.OnTouchListener getOnAddTouchListener() {
        return onAddTouchListener;
    }

    public View.OnTouchListener getOnNoteForLineTouchListener() {
        return onNoteForLineTouchListener;
    }

    public View.OnTouchListener getOnDeleteNoteListener() {
        return onDeleteNoteListener;
    }
}
