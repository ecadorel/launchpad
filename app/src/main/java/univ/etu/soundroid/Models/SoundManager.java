package univ.etu.soundroid.Models;

import java.util.*;

import android.app.Activity;
import android.media.*;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

public class SoundManager implements IXMLSerializable
{
    private HashMap<Integer,MediaPlayer> lstInstances;
    private List<Sound> soundList;

    private SoundManager()
    {
        lstInstances = new HashMap<>();
        soundList = new ArrayList<>();
    }

    private static int IdCount = -1;

    private static SoundManager instance = null;

    public static synchronized SoundManager getInstance()
    {
        if (instance == null)
        { 	instance = new SoundManager();
        }
        return instance;
    }

    public void stop(int idInstance){
        try {
            MediaPlayer mp = lstInstances.get(idInstance);

            if (mp != null && mp.isPlaying()) {
                mp.stop();
                mp.release();
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void setVolume(float i1,float i2){
        for(final MediaPlayer mp : lstInstances.values()){
            if(mp.isPlaying()){
                mp.setVolume(i1,i2);
            }
        }
    }

    public void setLooping(int idInstance, boolean looping) {
        MediaPlayer mp = lstInstances.get(idInstance);
        mp.setLooping(looping);
    }

    public synchronized int addSound(String path){
        for(int i = 0; i < soundList.size(); i++) {
            Sound s = soundList.get(i);
            if(s.isOnSdcard() && s.getPath().equals(path)) {
                return i;
            }
        }

        Sound s = new Sound(path);
        soundList.add(s);
        return soundList.size()-1;
    }

    public synchronized int addSound(int idRes){
        for(int i = 0; i < soundList.size(); i++) {
            Sound s = soundList.get(i);
            if(!s.isOnSdcard() && s.getIdResource() == idRes) {
                return i;
            }
        }

        Sound s = new Sound(idRes);
        soundList.add(s);
        return soundList.size()-1;
    }

    public void clearSoundList(){
        soundList.clear();
    }

    public void addInstance(MediaPlayer mp) {
        IdCount ++;
        lstInstances.put(IdCount, mp);
    }

    public int getIdCount() {
        return IdCount;
    }

    public Sound getSound(int idSound) {
        if(idSound < soundList.size() && idSound >= 0) {
            return soundList.get(idSound);
        } else {
            return null;
        }
    }

    public List<Sound> getSoundList() {
        return soundList;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element soundListElement = stream.getDocument().createElement("SoundList");
        for (Sound sound: soundList) {
            sound.serialize(stream,soundListElement);
        }
        root.appendChild(soundListElement);
    }

    @Override
    public void deserialize(XMLStream stream,Element root) {
        NodeList soundNodes = root.getChildNodes();
        soundList.clear();
        lstInstances.clear();
        for(int i=0;i<soundNodes.getLength();i++){
            if(soundNodes.item(i) instanceof Element) {
                Sound sound = new Sound();
                sound.deserialize(stream, (Element)soundNodes.item(i));
                soundList.add(sound);
            }
        }
    }
}