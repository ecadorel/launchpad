package univ.etu.soundroid.Models;

import android.util.ArraySet;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import univ.etu.soundroid.Controller.GridPlayController;
import univ.etu.soundroid.Controller.NoteController;

/**
 * Created by emile on 20/10/2015.
 */
public class Conductor extends Thread {

    private final Lock m_lock = new ReentrantLock(true);

    /*Le timer du conductor*/
    private MyTimer m_timer = new MyTimer();

    /*La liste de note a jouer dans le prochain tick*/
    public List<Integer> m_toPlay = new ArrayList<>();

    /*La liste des notes a arreter*/
    public List<Integer> m_toStop = new ArrayList<>();

    /*Le temps a attendre entre 2 frame*/
    private static final double FRAME_TIME = 1.0;

    private boolean close = false;

    public Conductor() {
        super();
    }

    /**
     * La routine globale du conductor, c'est cette methode qui va jouer toutes les notes
     * */
    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        double elapsed_time = 0;
        m_timer.start();
        while(!close) {
            m_timer.tick();

            if(m_timer.time() - elapsed_time > FRAME_TIME) {
                System.out.println(m_timer.time() - elapsed_time);
                m_lock.lock();

                elapsed_time = m_timer.time();

                for (int i = 0; i < m_toPlay.size(); i++) {
                    int note_id = m_toPlay.get(i);
                    NoteController.getInstance().playNote(note_id, m_timer.time());
                    if (NoteController.getInstance().isEnd(note_id, m_timer.time())) {
                        NoteController.getInstance().stop(note_id);
                        int index = m_toPlay.indexOf(note_id);
                        while(index != -1) {
                            m_toPlay.remove(index);
                            index = m_toPlay.indexOf(note_id);
                        }

                    }
                }

                for (int i = 0; i < m_toStop.size(); i++) {
                    int note_id = m_toStop.get(i);
                    if(NoteController.getInstance().isOnlySound(note_id)) {
                        NoteController.getInstance().stop(note_id);
                        int index = m_toPlay.indexOf(m_toStop.get(i));
                        while(index != -1) {
                            m_toPlay.remove(index);
                            index = m_toPlay.indexOf(m_toStop.get(i));
                        }
                        m_toStop.remove(i);
                    }
                }
                m_lock.unlock();
            }
        }

        //on arrete toutes les notes qui pourrait etre en train de jouer
        for (Map.Entry<Integer, Note> entry : GlobalData.getInstance().getGrid().getEntrySet()) {
            NoteController.getInstance().stop(entry.getValue().getId());
        }


    }

    /**
     * Retourne une note en fonction de son identifiant
     * @warning ThreadSafe
     * @throws IndexOutOfBoundsException [IndexOutOfBoundsException .length - 1]
     * */
    public Note getNoteById(int id) throws IndexOutOfBoundsException {
        m_lock.lock();
        Note n = GlobalData.getInstance().getGrid().getNote(id);
        m_lock.unlock();
        return n;
    }

    /**
     * Ajoute un son dans la liste des son a jouer
     * @warning ThreadSafe
     * @param soundIndex l'identifiant du son
     * */
    public void addSoundToPlay(int soundIndex) {
        m_lock.lock();
        m_toPlay.add(soundIndex);
        m_lock.unlock();
    }

    /**
     * Fonction appeler uniquement depuis run, fais planter le programme dans le cas contraire !!
     * @warning not ThreadSafe
     * @param soundIndex l'identifiant de la note
     * */
    public void addSoundToPlayInThread(int soundIndex) {
        m_toPlay.add(soundIndex);
    }

    /**
     * Demande l'arret d'une note
     * @warning ThreadSafe
     * @param soundIndex l'identifiant du son a arreter
     * */
    public void addSoundToStop(int soundIndex) {
        m_lock.lock();
        m_toStop.add(soundIndex);
        m_lock.unlock();
    }

    /**
     * Remet le timer a 0, ne supprime pas le note
     * Pour supprimer le notes faire setNbNote(0);
     * @warning ThreadSafe
     * */
    public void reset() {
        m_lock.lock();
        m_timer.start();
        m_lock.unlock();
    }

    public void stopThread() {
        m_lock.lock();
        close = true;
        m_lock.unlock();
    }

}
