package univ.etu.soundroid.Models;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

/**
 * Created by emile on 28/11/2015.
 */
public class NoteGrid implements IXMLSerializable{

    private Map<Integer, Note> m_notes = new HashMap<>();

    public NoteGrid() {
    }

    public Note addNote() {
        int maxId = -1;
        for(Map.Entry<Integer, Note> entry : m_notes.entrySet()) {
            if(maxId < entry.getKey()) maxId = entry.getKey();
        }
        Note note = new Note(maxId + 1);
        m_notes.put(maxId + 1, note);
        return note;
    }

    public  int count() {
        return m_notes.size();
    }

    public Set<Map.Entry<Integer, Note>> getEntrySet() {
        return m_notes.entrySet();
    }

    public Note getNote(int id) {
        return m_notes.get(id);
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element noteGrid = stream.getDocument().createElement("NoteGrid");
        for(Map.Entry<Integer, Note> entry: m_notes.entrySet()){
            entry.getValue().serialize(stream, noteGrid);
        }
        root.appendChild(noteGrid);
    }

    @Override
    public void deserialize(XMLStream stream,Element root) {
        NodeList notes = root.getChildNodes();
        m_notes.clear();
        for(int i=0;i<notes.getLength();i++){
            if(notes.item(i) instanceof Element) {
                Note note = new Note();
                note.deserialize(stream, (Element) notes.item(i));
                m_notes.put(note.getId(), note);
            }
        }
    }

    public void removeNote(int note) {
        m_notes.remove(note);
        for(Map.Entry<Integer, Note> entry : m_notes.entrySet()) {
            if(entry.getValue().getData().isPartition()) {
                entry.getValue().getData().getPartition().getPartitionData().remove(note);
            }
        }
    }
}
