package univ.etu.soundroid.Models;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by emile on 26/12/2015.
 */
public class FilePath {

    /**
     * Retourne une liste de fichier et de dossier contenu dans le path
     * @param path l'emplacement du dossier a lire
     * @return la liste des nom de fichier et de dossier contenu dans le path
     * */
    public List<String> getFileName(String path){

        if(path.lastIndexOf("/") != path.length() - 1) {
            path += "/";
        }

        File f = new File(path);
        if(!f.exists()) {
            f.mkdirs();
        }
        File file[] = f.listFiles();
        List<String> filenames = new ArrayList<>();
        if(!"/".equals(path)) filenames.add(path + "../");

        if(file != null) {
            for (int i = 0; i < file.length; i++) {
                filenames.add(file[i].getAbsolutePath());
            }
        }

        return filenames;
    }

    /**
     * Retourne la liste des fichiers contenu dans le dossier et respectant l'extension, retoure aussi les dossier
     * @param path l'emplacement du dossier a consulter
     * @param ext la liste des extensions a conserver
     * */
    public List<String> getFileName(String path, final String[] ext) {

        if(path.lastIndexOf("/") != path.length() - 1) {
            path += "/";
        }

        File f = new File(path);
        if(!f.exists()) {
            f.mkdirs();
        }
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if(pathname.isDirectory()) return true;
                else {
                    String s = pathname.getPath();
                    String[] e = pathname.getPath().split(Pattern.quote("."));
                    if(e.length > 1) {
                        for (int i = 0; i < ext.length; i++) {
                            if (("." + e[1]).equals(ext[i]) || ext[i].equals(".*")) return true;
                        }
                    }
                }
                return false;
            }
        };

        File file[] = f.listFiles(filter);
        List<String> filenames = new ArrayList<>();
        filenames.add(path + "../");
        if(file != null) {
            for (int i = 0; i < file.length; i++) {
                filenames.add(file[i].getAbsolutePath());
            }
        }

        return filenames;
    }

}
