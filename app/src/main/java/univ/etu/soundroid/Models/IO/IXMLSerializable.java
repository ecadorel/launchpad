package univ.etu.soundroid.Models.IO;

import org.w3c.dom.Element;

/**
 * Created by Jimmy on 25/11/2015.
 */
public interface IXMLSerializable {

    public void serialize(XMLStream stream,Element root);

    public void deserialize(XMLStream stream,Element root);
}
