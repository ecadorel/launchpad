package univ.etu.soundroid.Models;

import org.w3c.dom.Element;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

/**
 * Created by thenawaker on 23/10/2015.
 */
public class Sound implements IXMLSerializable{
    private String path;
    private int idResource = -1;
    boolean onSdcard;

    public Sound(){}

    public Sound(int id){
        idResource = id;
        onSdcard = false;
    }

    public Sound(String p){
        path=p;
        onSdcard = true;
    }

    public boolean isOnSdcard() {
        return onSdcard;
    }

    public int getIdResource() {
        return idResource;
    }

    public String getPath() {
        return path;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element sound = stream.getDocument().createElement("Sound");
        sound.setAttribute("resourceId",Integer.toString(idResource));
        sound.setAttribute("externalSound",Boolean.toString(onSdcard));
        if(path!=null)
            sound.setAttribute("path",path);
        root.appendChild(sound);
    }

    @Override
    public void deserialize(XMLStream stream,Element root) {
        idResource = Integer.parseInt(root.getAttribute("resourceId"));
        onSdcard = Boolean.parseBoolean(root.getAttribute("externalSound"));
        if(root.hasAttribute("path"))
            path = root.getAttribute("path");
    }
}
