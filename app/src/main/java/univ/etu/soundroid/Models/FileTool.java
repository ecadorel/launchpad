package univ.etu.soundroid.Models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.os.Environment;
import java.io.File;

/**
 * Created by Jimmy on 17/10/2015.
 */
public class FileTool {

    public static File SdCard = android.os.Environment.getExternalStorageDirectory();

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static void registerFile(Activity activity, String path){
        MediaScannerConnection.scanFile(activity, new String[]{path}, null, null);
    }

    public static void showFileChooser(Activity activity,int code) throws android.content.ActivityNotFoundException {
        showFileChooser(activity,code,"SÃ©lÃ©ctionnez un fichier","*/*");
    }

    public static void showFileChooser(Activity activity,int code,String message,String type) throws android.content.ActivityNotFoundException {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        activity.startActivityForResult(Intent.createChooser(intent, message), code);
    }
}