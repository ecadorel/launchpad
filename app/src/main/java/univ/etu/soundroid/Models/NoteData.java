package univ.etu.soundroid.Models;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

/**
 * Created by emile on 28/11/2015.
 */
public class NoteData implements IXMLSerializable{

    private int color = 0x0FF000;
    private boolean isPartition = false;
    private int soundId = 0;
    private Partition partition = new Partition();

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isPartition() {
        return isPartition;
    }

    public void setIsPartition(boolean isPartition) {
        this.isPartition = isPartition;
    }

    public int getSoundId() {
        return soundId;
    }

    public void setSoundId(int soundId) {
        this.soundId = soundId;
    }

    public Partition getPartition() {
        return partition;
    }

    public void setPartition(Partition partition) {
        this.partition = partition;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element noteData = stream.getDocument().createElement("NoteData");
        noteData.setAttribute("color",Integer.toString(color));
        noteData.setAttribute("isPartition",Boolean.toString(isPartition));
        noteData.setAttribute("soundId",Integer.toString(soundId));
        if(isPartition)
            partition.serialize(stream,noteData);
        root.appendChild(noteData);

    }

    @Override
    public void deserialize(XMLStream stream,Element root) {
        color = Integer.parseInt(root.getAttribute("color"));
        isPartition = Boolean.parseBoolean(root.getAttribute("isPartition"));
        soundId = Integer.parseInt(root.getAttribute("soundId"));
        if(isPartition) {
            NodeList nodes = root.getChildNodes();
            for(int i = 0; i < nodes.getLength(); i++) {
                if(nodes.item(i) instanceof Element) {
                    partition.deserialize(stream, (Element)nodes.item(i));
                }
            }
        }
    }
}
