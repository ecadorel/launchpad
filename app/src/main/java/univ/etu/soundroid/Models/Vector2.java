package univ.etu.soundroid.Models;

import org.w3c.dom.Element;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

public class Vector2 implements IXMLSerializable {


    public Vector2() {}


    public Vector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element vec = stream.getDocument().createElement("Vector2");
        vec.setAttribute("x",Integer.toString(x));
        vec.setAttribute("y",Integer.toString(y));
        root.appendChild(vec);
    }

    @Override
    public void deserialize(XMLStream stream,Element root) {
        x = Integer.parseInt(root.getAttribute("x"));
        y = Integer.parseInt(root.getAttribute("y"));
    }
}