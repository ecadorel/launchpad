package univ.etu.soundroid.Models;

import android.app.Application;
import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emile on 24/12/2015.
 * Cette classe contient toutes les informations utile a soundroid qui ne font pas partis d'un projet
 *
 * */
public class SoundroidAppData extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        SoundroidAppData.context = getApplicationContext();
        try {
            load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Context getAppContext() {
        return SoundroidAppData.context;
    }

    private static List<ProjectFile> recentProjects = new ArrayList<>();

    public static void addRecentProject(ProjectFile file){
        if(!recentProjects.contains(file)) {
            recentProjects.add(0, file);
            cleanList();
            save();
        }else if(!recentProjects.get(0).equals(file)){
            int index = recentProjects.indexOf(file);
            recentProjects.remove(index);
            recentProjects.add(0,file);
        }
    }

    private static void cleanList(){
        if(recentProjects.size() > 10) {
            int size = recentProjects.size();
            for(int i = 10; i < size; i++) {
                recentProjects.remove(10);
            }
        }
    }

    public static List<ProjectFile> getRecentProject(){
        return recentProjects;
    }

    public static void save(){
        List<String> pathList = new ArrayList<>();
        for (ProjectFile file : recentProjects) {
            pathList.add(file.getPath());
        }
        try{
            FileOutputStream fos = context.openFileOutput("RecentProjects.dat", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(pathList);
            os.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void load() throws IOException {
        FileInputStream fis = context.openFileInput("RecentProjects.dat");
        ObjectInputStream is = new ObjectInputStream(fis);
        try {
            List<String> pathList = (ArrayList<String>) is.readObject();
            for (String path : pathList) {
                recentProjects.add(new ProjectFile(path));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        is.close();
        fis.close();
    }
}
