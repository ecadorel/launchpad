package univ.etu.soundroid.Models;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

/**
 * Created by emile on 29/11/2015.
 */
public class Partition implements IXMLSerializable{

    private int len = 2000;
    private Map<Integer, List<Vector2>> partitionData = new HashMap<>();

    public int count() {
        return partitionData.size();
    }

    public List<Vector2> getLine(int i) {
        return partitionData.get(i);
    }

    public Map<Integer, List<Vector2>> getPartitionData() {
        return partitionData;
    }

    public void setPartitionData(Map<Integer, List<Vector2>> partitionData) {
        this.partitionData = partitionData;
    }

    public void addLine(int id) {
        if(!partitionData.containsKey(id)) {
            partitionData.put(id, new ArrayList<Vector2>());
        }
    }


    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element partition = stream.getDocument().createElement("Partition");
        partition.setAttribute("len",Integer.toString(len));
        Element partitionDataElement = stream.getDocument().createElement("PartitionData");
        for(Map.Entry<Integer, List<Vector2>> entry : partitionData.entrySet()){
            Element dataList = stream.getDocument().createElement("DataList");
            dataList.setAttribute("id",Integer.toString(entry.getKey()));
            for(Vector2 vec : entry.getValue()){
                vec.serialize(stream,dataList);
            }
            partitionDataElement.appendChild(dataList);
        }
        partition.appendChild(partitionDataElement);
        root.appendChild(partition);
    }

    @Override
    public void deserialize(XMLStream stream, Element root) {
        len = Integer.parseInt(root.getAttribute("len"));
        NodeList partitionDataNode = root.getChildNodes();
        for(int i = 0; i < partitionDataNode.getLength(); i++) {
            if(partitionDataNode.item(i) instanceof Element) {
                partitionDataNode = partitionDataNode.item(i).getChildNodes();
                break;
            }
        }
        partitionData.clear();
        for(int i=0;i<partitionDataNode.getLength();i++){
            if(partitionDataNode.item(i) instanceof Element) {
                List<Vector2> dataList = new ArrayList<>();
                Element dataListElement = (Element) partitionDataNode.item(i);
                int id = Integer.parseInt(dataListElement.getAttribute("id"));
                NodeList vectors = dataListElement.getChildNodes();
                for (int j = 0; j < vectors.getLength(); j++) {
                    if(vectors.item(j) instanceof Element) {
                        Vector2 vec = new Vector2();
                        vec.deserialize(stream, (Element) vectors.item(j));
                        dataList.add(vec);
                    }
                }
                partitionData.put(id, dataList);
            }
        }
    }

    public void removeLine(int id) {
        partitionData.remove(id);
    }
}
