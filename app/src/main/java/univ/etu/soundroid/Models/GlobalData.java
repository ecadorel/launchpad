package univ.etu.soundroid.Models;

import org.w3c.dom.Element;

import java.io.File;
import java.util.regex.Pattern;

import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

/**
 * Created by emile on 28/11/2015.
 * Une sorte de session
 */
public class GlobalData implements IXMLSerializable {

    public static final String BASIC_PATH = FileTool.SdCard.getAbsolutePath() + "/Documents/SoundroidProjects/";
    private static volatile GlobalData instance = null;
    private ProjectFile file;
    private NoteGrid grid;

    private GlobalData() {}

    public static GlobalData getInstance() {
        if(instance == null) {
            instance = new GlobalData();
        }
        return instance;
    }

    public void setDefaultProjectFile(String name){
        file = new ProjectFile(BASIC_PATH + name + ".xml");
    }

    public void setProjectFile(String fullpath) {
        file = new ProjectFile(fullpath);
    }

    public void setProjectFile(File f){
        file = new ProjectFile(f);
    }

    public ProjectFile getProjectFile(){
        return file;
    }

    public NoteGrid getGrid() {
        return grid;
    }

    public void setGrid(NoteGrid grid) {
        this.grid = grid;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element global = stream.getDocument().createElement("GlobalData");
        if(grid!=null) {
            grid.serialize(stream, global);
        }
        SoundManager.getInstance().serialize(stream,global);
        root.appendChild(global);
    }

    @Override
    public void deserialize(XMLStream stream, Element root) {
        grid = new NoteGrid();
        if (root.getFirstChild() != null && root.getChildNodes().getLength() >= 3){
            grid.deserialize(stream, (Element)root.getChildNodes().item(1));
            SoundManager.getInstance().deserialize(stream, (Element) root.getChildNodes().item(3));
        } else {
            SoundManager.getInstance().deserialize(stream, (Element) root.getChildNodes().item(1));
        }
    }

    public void clear(){
        grid = null;
    }
}
