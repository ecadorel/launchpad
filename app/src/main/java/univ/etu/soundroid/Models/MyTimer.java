package univ.etu.soundroid.Models;

public class MyTimer
{
    private double mBaseTime     = 0;
    private double mDeltaTime    = 0;
    private double mPreviousTime = 0;
    private double mCurrentTime  = 0;

    /**
     * Calcul un nouveau temps
     * */
    public void tick() {
        mCurrentTime = System.currentTimeMillis();
        mDeltaTime = mCurrentTime - mPreviousTime;
        mPreviousTime = mCurrentTime;
    }

    /*
    * A appeler avant de faire des ticks
    * Remet le timer a 0
    * */
    public void start() {
        mBaseTime = mPreviousTime = mCurrentTime = System.currentTimeMillis();
    }

    /**
     *	Temps total
     *	@return le temps total depuis le dernier start
     */
    public double time() {
        return mCurrentTime - mBaseTime;
    }

    /**
     *	Temps entre deux ticks
     *	@return le temps entre les deux precedents tick
     */
    public double deltaTime() {
        return mDeltaTime;
    }

}
