package univ.etu.soundroid.Models;

import java.io.File;

/**
 * Created by Jimmy on 10/01/2016.
 */

public class ProjectFile{
    File file;

    public ProjectFile(File f){
        file=f;
    }

    public ProjectFile(String path){
        file = new File(path);
    }

    public String getPath(){
        return file.getPath();
    }

    public String getName() {
        return file.getName();
    }

    public File getFile(){
        return file;
    }

    @Override
    public boolean equals(Object o) {
        boolean res = false;

        if (o instanceof ProjectFile) {
            ProjectFile pf = (ProjectFile) o;
            if (pf.getFile() != null || this.file != null) {
                res = pf.getFile().getPath().equals(this.getPath());
            }
        }
        return res;
    }


    @Override
    public int hashCode(){
        int hash = 7;
        hash = 17 * hash + (this.getFile() != null && !this.getFile().getPath().isEmpty() ? this.getFile().getPath().hashCode() : 0);
        return hash;
    }
}
