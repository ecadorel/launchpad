package univ.etu.soundroid.Models;

import android.util.Pair;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;

import univ.etu.soundroid.Controller.GridPlayController;
import univ.etu.soundroid.Controller.SoundController;
import univ.etu.soundroid.Models.IO.IXMLSerializable;
import univ.etu.soundroid.Models.IO.XMLStream;

/**
 * Created by emile on 28/11/2015.
 */
public class Note implements IXMLSerializable{

    private int m_id;

    private NoteData data = new NoteData();

    public Note(){}

    public Note(int id) {
        m_id = id;
    }

    public NoteData getData() {
        return data;
    }

    public void setData(NoteData data) {
        this.data = data;
    }

    public int getId() {
        return m_id;
    }

    @Override
    public void serialize(XMLStream stream, Element root) {
        Element note = stream.getDocument().createElement("Note");
        note.setAttribute("id",Integer.toString(m_id));
        data.serialize(stream, note);
        root.appendChild(note);
    }

    @Override
    public void deserialize(XMLStream stream,Element root) {
        m_id = Integer.parseInt(root.getAttribute("id"));
        NodeList nodes = root.getChildNodes();
        for(int i = 0; i < nodes.getLength(); i++) {
            if(nodes.item(i) instanceof Element) {
                data.deserialize(stream, (Element)nodes.item(i));
                break;
            }
        }
    }
}
