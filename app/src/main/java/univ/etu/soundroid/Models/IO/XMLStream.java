package univ.etu.soundroid.Models.IO;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Jimmy on 25/11/2015.
 */
public class XMLStream {

    private Document document;
    private Element root;

    public XMLStream(){}

    public void openStream(File file) throws ParserConfigurationException, IOException, SAXException, DocumentMalformedException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder loader = factory.newDocumentBuilder();
        document = loader.parse(file);
        NodeList node = document.getElementsByTagName("XMLStreamData");
        if(node!=null){
            for(int i = 0 ; i < node.getLength(); i++) {
                if(node.item(i) instanceof Element) {
                    root = (Element) node.item(i);
                    break;
                }
            }
        }else{
            throw new DocumentMalformedException(); // cas ou il n'y a pas de root
        }
    }

    public void serialize(IXMLSerializable xml){
        xml.serialize(this,root);
    }

    public void deserialize(IXMLSerializable xml) throws DocumentMalformedException {
        NodeList elements = root.getChildNodes();
        if(elements !=null)
            for(int i = 0; i < elements.getLength(); i++) {
                if(elements.item(i) instanceof Element) {
                    xml.deserialize(this, (Element) elements.item(i));
                    break;
                }
            }
        else
            throw new DocumentMalformedException();
    }

    public void initStream() throws ParserConfigurationException {
        DocumentBuilderFactory fabrique;
        fabrique = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = fabrique.newDocumentBuilder();
        document = builder.newDocument();
        root = getDocument().createElement("XMLStreamData");
    }

    public void createStream(File file, IXMLSerializable xml) throws ParserConfigurationException, TransformerException {
        initStream();
        serialize(xml);
        getDocument().appendChild(getRoot());
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(document);
        StreamResult fileStream = new StreamResult(file);
        transformer.transform(source, fileStream);
    }

    public Document getDocument() {
        return document;
    }

    public Element getRoot() {
        return root;
    }
}
